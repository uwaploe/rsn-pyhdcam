#!/usr/bin/env python
from distutils.core import setup
from distutils.command.sdist import sdist
import os
import codecs
import re


here = os.path.abspath(os.path.dirname(__file__))

# Read the version number from a source file.
# Why read it, and not import?
# see https://groups.google.com/d/topic/pypa-dev/0PkjVpcxTzQ/discussion
def find_version(*file_paths):
    # Open in Latin-1 so that we avoid encoding errors.
    # Use codecs.open for Python 2 compatibility
    with codecs.open(os.path.join(here, *file_paths), 'r', 'latin1') as f:
        version_file = f.read()

    # The version line must have the form
    # __version__ = 'ver'
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return str(version_match.group(1))
    raise RuntimeError("Unable to find version string.")


# Adapted from http://guide.python-distribute.org/specification.html#development-releases
class my_sdist(sdist):
    user_options = sdist.user_options + [
        ('dev', None, 'Add development version tag')
    ]

    def initialize_options(self):
        sdist.initialize_options(self)
        self.dev = 0

    def run(self):
        if self.dev:
            suffix = '.dev%s' % self.get_head_revision()
            self.distribution.metadata.version += suffix
        sdist.run(self)

    def get_head_revision(self):
        from subprocess import Popen, PIPE
        cmdline = 'git log -n 1 --pretty=oneline'
        output = Popen(cmdline.split(), stdout=PIPE).communicate()[0]
        rev = (output.strip().split())[0]
        return rev[0:7]


setup(name="Pyhdcam",
      version=find_version('pyhdcam', '__init__.py'),
      cmdclass={'sdist': my_sdist},
      description="RSN HD Camera System software",
      author="Michael Kenney",
      author_email="mikek@apl.uw.edu",
      url="http://wavelet.apl.uw.edu/~mike/python/",
      packages=["pyhdcam", "pyhdcam.tcm6"],
      scripts=["bin/ncdtest.py",
               "bin/tcm6test.py",
               "bin/client_test.py",
               "bin/server_test.py",
               "bin/relay_stress.py",
               "bin/pt_test.py",
               "bin/camtest.py",
               "bin/camserver.py",
               "bin/camclient.py"],
      data_files=[('share/hdcam', ['bin/hdcamctl.yaml',
                                   'bin/pt_only.yaml'])])
