#!/usr/bin/python
#
# Distribute the Pyhdcam package and the Runit service script to
# the encode and decode computers.
#
from fabric.api import *
import os


env.roledefs = {
    'repo': ['wavelet.apl.uw.edu'],
    'encode': ['rsn@hdcam-local', 'hdcam-local'],
    'decode': ['rsn@rsn-hdproc-2.apl.uw.edu', 'rsn-hdproc-2.apl.uw.edu']
}


def prepare():
    version = open('version.txt', 'r').read().strip()
    local('python setup.py sdist')
    with lcd('svc'):
        local('tar -c -v -z -f hdcamctl.tar.gz hdcamctl')
    env.archive = 'dist/Pyhdcam-{0}.tar.gz'.format(version)
    env.svc_archive = 'svc/hdcamctl.tar.gz'


@roles('repo')
def distribute():
    put(env.archive, 'public_html/python')


@roles('decode', 'encode')
def deploy():
    if env.host == 'hdcam-local':
        gw = 'rsn-hdproc-2.apl.uw.edu'
    else:
        gw = None
    with settings(gateway=gw):
        put(env.archive)
        filename = os.path.basename(env.archive)
        if env.user == 'mike':
            if env.host == 'hdcam-local':
                with prefix('source .venvs/hdcam/bin/activate'):
                    run('pip install --upgrade {0}'.format(filename))
            else:
                with prefix('source .venvs/rsn/bin/activate'):
                    run('pip install --upgrade {0}'.format(filename))
        else:
            run('pip install --upgrade {0}'.format(filename))
        run('rm -f {0}'.format(filename))
        if env.user == 'rsn' and env.host == 'hdcam-local':
            put('bin/hdcamctl.yaml')
            put(env.svc_archive, 'svc')
            with cd('svc'):
                run('tar xvzf ' + os.path.basename(env.svc_archive))
            #with cd('service'):
            #    run('sv restart ./hdcamctl')
