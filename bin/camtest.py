#!/usr/bin/env python
"""
%prog serialdev [logfile]

Interactive interface to the SubC 1Cam
"""
import sys
import urwid
from textwrap import dedent
import curses.ascii as ascii
from pyhdcam.camera import Commands, Camera
from pyhdcam.serialcom import Port
import simplejson as json
import os


cmd_dict = {
    'up': Commands.MENU_UP,
    'down': Commands.MENU_DOWN,
    'left': Commands.MENU_LEFT,
    'right': Commands.MENU_RIGHT,
    'enter': Commands.MENU_SELECT,
    's': Commands.MENU_SELECT,
    '+': Commands.ZOOM_IN,
    '-': Commands.ZOOM_OUT,
}

palette = [
    ('body', 'black', 'light gray', 'standout'),
    ('header', 'black', 'white', 'bold'),
    ('key', 'yellow', 'black', 'bold', '#fd0', '#000'),
    ('cmd', 'white', 'black', 'standout')
]

help_text = [
    [('key', 'M'), ' : ', ('cmd', 'Show Menu')],
    [('key', 'up arrow'), ' : ', ('cmd', 'Menu Up')],
    [('key', 'down arrow'), ' : ', ('cmd', 'Menu Down')],
    [('key', 'left arrow'), ' : ', ('cmd', 'Menu Left')],
    [('key', 'right arrow'), ' : ', ('cmd', 'Menu Right')],
    [('key', 'enter'), ' : ', ('cmd', 'Menu Select')],
    [('key', '+'), ' : ', ('cmd', 'Zoom In')],
    [('key', '-'), ' : ', ('cmd', 'Zoom Out')],
    [('key', 'v'), ' : ', ('cmd', 'Toggle Video Mode')],
    [('key', 'l'), ' : ', ('cmd', 'Toggle Lasers')],
    [('key', '0'), ' : ', ('cmd', 'Toggle Power')]
]


def input_handler(key):
    if key in ('q', 'Q', 'ctrl d', 'esc'):
        raise urwid.ExitMainLoop()


class CommandInput(urwid.Edit):
    def __init__(self, commands, cam, **kwds):
        self.commands = commands
        self.cam = cam
        urwid.Edit.__init__(self, 'Response: ')

    def keypress(self, size, key):
        cmd = self.commands.get(key)
        self.move_cursor_to_coords(size, self.edit_pos, 0)
        if cmd:
            resp = self.cam.send(cmd, timeout=5)
            self.set_edit_text(resp or '')
            return None
        else:
            return key


def app(cam, logfile=None):
    codes = [getattr(Commands, c) for c in dir(Commands)
             if ascii.isupper(c[0])]
    for code, feedback in codes:
        cmd_dict[code[-1]] = (code, feedback)
    # Setup widgets
    hdr = urwid.AttrMap(urwid.Text('Enter 1Cam command codes'), 'header')
    items = [urwid.Text(e) for e in help_text]
    widths = [len(w.text) for w in items]
    help_box = urwid.GridFlow(items, max(widths), 2, 0, 'left')
    body = urwid.Pile([help_box,
                       urwid.AttrMap(CommandInput(cmd_dict, cam), 'body')],
                      focus_item=1)
    footer = urwid.Text('<ESC> or q to exit')
    frame = urwid.Frame(urwid.Filler(body, 'top'),
                        header=hdr, footer=footer)
    frame.focus_position = 'body'
    loop = urwid.MainLoop(frame,
                          palette=palette,
                          unhandled_input=input_handler)
    if os.environ['TERM'].startswith('xterm'):
        loop.screen.set_terminal_properties(colors=256)
    if logfile:
        cam.start_logging()
        loop.run()
        log = cam.stop_logging()
        json.dump(log, logfile)
    else:
        loop.run()


def main():
    try:
        devname = sys.argv[1]
    except IndexError:
        sys.stderr.write(dedent(__doc__))
        sys.exit(1)
    p = Port(devname, baud=9600)
    p.open()
    cam = Camera(p)
    cam.flush_input()
    if len(sys.argv) > 2:
        with open(sys.argv[2], 'r') as f:
            app(cam, logfile=f)
    else:
        app(cam)


if __name__ == '__main__':
    main()
