#!/usr/bin/env python
#
"""%prog [command [command ...]]

Command interpreter interface to the HDCAM server. Use the 'help' command
for more details:

    %prog help
"""
import zmq
import simplejson as json
import shlex
import cmd2
import subprocess
from optparse import OptionParser
from ast import literal_eval

UV_TEMPLATE = 'uv -d decklink:0 -m {mtu:d} {endpoint}'


def args_to_dict(args, do_eval=True):
    """
    Convert a command argument list of the form NAME=VALUE
    into a dictionary.

    >>> d = args_to_dict('foo=bar baz=1,2,3').items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar')]
    """
    def maybe_eval(s):
        if do_eval:
            try:
                return literal_eval(s)
            except (ValueError, SyntaxError):
                return s
        else:
            return s
    params = {}
    for name, val in [arg.split('=') for arg in args.split()]:
        if ',' in val:
            params[name] = [maybe_eval(v) for v in val.split(',')]
        else:
            params[name] = maybe_eval(val)
    return params


def send_msg(sock, msg, timeout=2500):
    """
    Send a multi-part message to a ZeroMQ endpoint and return the response.

    :param sock: socket object
    :type sock: zmq.core.socket.Socket
    :param msg: message contents
    :param timeout: maximum time (ms) to wait for a response
    :return: multi-part response
    :rtype: tuple or ``None``
    """
    result = None
    poll = zmq.Poller()
    poll.register(sock)
    sock.send_multipart(msg)
    socks = dict(poll.poll(timeout))
    if socks.get(sock) == zmq.POLLIN:
        result = sock.recv_multipart()
    poll.unregister(sock)
    return result


def subscription(sock, sentinel, timeout=2500):
    """
    Generator to return messages from a SUB socket until *sentinel*
    is seen or *timeout* expires.
    """
    poll = zmq.Poller()
    poll.register(sock)
    socks = dict(poll.poll(timeout))
    while socks.get(sock) == zmq.POLLIN:
        result = sock.recv_multipart()
        if result[0] != sentinel:
            yield result
            socks = dict(poll.poll(timeout))
        else:
            break


class HdcamApp(cmd2.Cmd):
    """
    Simple command interpreter for testing the HDCAM server

    Commands (type help <topic>)
    =================================================
    open close start stop lookat lights camera adread
    quit
    """
    prompt = 'HDCAM> '
    socket = None
    ctx = None
    host = None
    proc = None
    timeout = 30

    def _open(self, host):
        if self.ctx is None:
            self.ctx = zmq.Context()
        self.host = host
        self.socket = self.ctx.socket(zmq.REQ)
        self.sub = self.ctx.socket(zmq.SUB)
        self.sub.setsockopt(zmq.SUBSCRIBE, 'INMOTION')
        self.sub.setsockopt(zmq.SUBSCRIBE, 'STOPPED')
        self.socket.connect('tcp://{0}:5500'.format(host))
        self.sub.connect('tcp://{0}:5501'.format(host))

    def _close(self):
        self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.close()
        self.sub.setsockopt(zmq.LINGER, 0)
        self.sub.close()
        self.socket = None
        self.sub = None

    def _reopen(self):
        assert self.host is not None
        self._close()
        self._open(self.host)

    def _send(self, msg):
        result = send_msg(self.socket, msg, self.timeout * 1000)
        if result is None:
            self.perror('No response from server. Reconnecting ...\n')
            self._reopen()
            result = None, None
            if self.proc:
                self.pfeedback('Stopping Ultragrid process')
                self.proc.terminate()
                self.proc = None
        return result

    def do_help(self, arg):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            self.poutput(self.__doc__ + '\n')

    def do_open(self, args):
        """open HOST: open a connection to HOST."""
        if not args:
            self.perror('You must specify a hostname\n')
        else:
            self._open(args)

    def do_close(self, args):
        """close: close the network connection"""
        assert self.socket is not None
        self._close()

    def do_start(self, args):
        """start [PARAM=VALUE ...]: send a START message."""
        assert self.socket is not None
        params = args_to_dict(args, do_eval=False)
        resp, contents = self._send([
            'START',
            json.dumps(params)
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))
        if resp != 'ERROR':
            self.pfeedback('Starting local Ultragrid process')
            uv_cmd = UV_TEMPLATE.format(endpoint=self.host,
                                        mtu=int(params.get('mtu', 8500)))
            outf = open('/dev/null', 'w')
            try:
                self.proc = subprocess.Popen(
                    shlex.split(uv_cmd),
                    stdout=outf,
                    stderr=subprocess.STDOUT
                )
            except (OSError, ValueError) as e:
                self.perror(str(e) + '\n')

    def do_stop(self, args):
        """stop: send a STOP message."""
        assert self.socket is not None
        resp, contents = self._send([
            'STOP',
            b''
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))
        if self.proc:
            self.proc.terminate()
            self.proc = None

    def do_lookat(self, args):
        """lookat [PARAM=VALUE ...]: send a LOOKAT message."""
        assert self.socket is not None
        params = args_to_dict(args)
        resp, contents = self._send([
            'LOOKAT',
            json.dumps(params)
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))
        if args:
            for msg in subscription(self.sub, 'STOPPED'):
                resp, contents = msg
                self.poutput('{0}: {1}\n'.format(
                    resp,
                    contents
                ))

    def do_lights(self, args):
        """lights [i1 [i2]]: send a LIGHTS message."""
        assert self.socket is not None
        if args:
            vals = [int(a, 0) for a in args.split()]
            if len(vals) == 1:
                vals.append(vals[0])
            params = {'intensity': vals}
        else:
            params = {}
        resp, contents = self._send([
            'LIGHTS',
            json.dumps(params)
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))

    def do_camera(self, args):
        """camera [PARAM=VALUE ...]: send a CAMERA message."""
        assert self.socket is not None
        params = args_to_dict(args)
        resp, contents = self._send([
            'CAMERA',
            json.dumps(params)
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))

    def do_adread(self, args):
        """adread [PARAM=VALUE ...]: send an ADREAD message."""
        assert self.socket is not None
        params = args_to_dict(args)
        resp, contents = self._send([
            'ADREAD',
            json.dumps(params)
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))

    def do_error(self, args):
        """error: send an invalid message to test error handling."""
        assert self.socket is not None
        resp, contents = self._send([
            'FOO',
            b''
        ])
        if resp != 'ERROR':
            self.perror('Bad response from server ({0}, {1!r}\n'.format(
                resp, contents
            ))


def main():
    parser = OptionParser(usage=__doc__)
    parser.parse_args()
    obj = HdcamApp()
    obj.cmdloop()

if __name__ == '__main__':
    main()
