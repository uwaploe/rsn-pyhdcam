#!/bin/bash
#
# Run a camclient "batch file".
#
VENV=$HOME/.venvs/hdcam


file="$1"
[[ "$file" ]] || {
    echo "Usage: $(basename $0) cmdfile" 1>&2
    exit 1
}

# Initialize the Python virtual environment
. $VENV/bin/activate
export PATH=$PATH:/usr/local/bin

cd $HOME/Documents || cd $HOME

# Env variables for email alerts from camclient.py
#
# SMTP server to use
export SMTP_RELAY="10.20.1.9"
# Semicolon separated list of recipients
export CAMHD_ALERT="mikek@apl.uw.edu;mikeh@apl.washington.edu"

camclient.py "load $file" "quit" 1> $HOME/logs/CAMHDA301-`date \+\%Y\%m\%dT\%H\%M`00Z.log 2>&1

# Power cycle the camera
ssh -t hdcam-local \
    "source ~/.venvs/hdcam/bin/activate;ncdtest.py 'clear 0' quit" 1> /dev/null 2>&1
sleep 5
ssh -t hdcam-local \
    "source ~/.venvs/hdcam/bin/activate;ncdtest.py 'set 0' quit" 1> /dev/null 2>&1
