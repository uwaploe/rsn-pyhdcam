#!/usr/bin/env python
#
# ZeroMQ based server for HDCAM (experimental)
#
import sys
import shlex
import gevent
from gevent_zeromq import zmq
import simplejson as json
import logging
import subprocess
import time
import signal
from functools import partial
import pyhdcam.system as HDCAM

# Map input video resolution and frame-rate to mode-codes for the
# Ultragrid program
VMODES = {
    ('1080p', '29.97'): 6,
    ('1080p', '30'): 7,
    ('1080i', '59.94'): 9,
    ('1080i', '60'): 10,
    ('1080p', '59.94'): 12,
    ('1080p', '60'): 13,
    ('720p', '59.94'): 15,
    ('720p', '60'): 16
}

UV_TEMPLATE = 'uv -t decklink:0:{mode:d} -m {mtu:d} {endpoint}'


class Service(object):
    """
    Class to implement the server side of the HDCAM Control Protocol. Each
    command, CMD, is handled by a method named 'do_CMD'.
    """
    defaults = {'vres': '1080i',
                'vrate': '59.94',
                'mtu': 8500,
                'endpoint': ''}
    workdir = '/var/tmp'

    def __init__(self, cfg, async_cb, **kwds):
        """
        :param cfg: configuration dictionary
        :param async_cb: callback for asynchronous operations
        """
        self.logger = logging.getLogger('server')
        self.lights = HDCAM.Lights(cfg)
        self.lights.flash(0.25, [10, 10])
        self.pt = HDCAM.PanTilt(cfg, **kwds)
        self.lights.flash(0.25, [10, 10])
        self.async_cb = async_cb
        self.cfg = cfg
        self.proc = None
        self.monitor = None
        self.camera = None

    @property
    def orientation(self):
        """
        Camera orientation.
        """
        fb = self.pt.feedback()
        return {
            'heading': fb.heading,
            'pitch': fb.pitch,
            'pan': fb.pan,
            'tilt': fb.tilt,
            'timestamp': int(time.time()),
            'is_moving': (fb.pan_status & 1) | (fb.tilt_status & 1)
        }

    def _ptmon(self, heading, pitch, speed, cb):
        """
        Task to monitor the heading/pitch motion and pass the orientation
        information to a callback.
        """
        for t, status in self.pt.adjust(heading=heading, pitch=pitch,
                                        speed=speed):
            if isinstance(status, Exception):
                msg = 'ERROR'
                contents = str(status)
            else:
                contents = {'heading': status.heading,
                            'pitch': status.pitch,
                            'timestamp': int(t)}
                if (status.pan_status & 1) or (status.tilt_status & 1):
                    msg = 'INMOTION'
                else:
                    msg = 'STOPPED'
            cb((msg, contents))

    def dispatch(self, command, payload):
        f = getattr(self, 'do_' + command.upper(), None)
        if f:
            self.logger.info('Executing %s', command)
            self.logger.debug('payload: %r', payload)
            return f(payload)
        return 'ERROR', 'Invalid command'

    def do_START(self, payload):
        """
        Start the video streaming server.
        """
        if self.proc is not None:
            return 'ERROR', 'Process is already running'

        if 'endpoint' not in payload:
            return 'ERROR', 'No endpoint specified'

        params = dict(
            endpoint=payload['endpoint'],
            mtu=payload.get('mtu',
                            self.defaults['mtu'])
        )
        vres = payload.get('vres', self.defaults['vres'])
        vrate = payload.get('vrate', self.defaults['vrate'])
        params['mode'] = VMODES.get((vres, vrate))
        if not params['mode']:
            return 'ERROR', 'Invalid video mode'
        uv_cmd = UV_TEMPLATE.format(**params)
        outf = open('/dev/null', 'w')
        try:
            t = time.time()
            self.proc = subprocess.Popen(
                shlex.split(uv_cmd),
                stdout=outf,
                stderr=subprocess.STDOUT,
                cwd=self.workdir
            )
        except (OSError, ValueError) as e:
            self.logger.exception('START command failed')
            return 'ERROR', str(e)
        return 'STARTED', {'time': t, 'cmd': uv_cmd, 'pid': self.proc.pid}

    def do_STOP(self, payload):
        """
        Stop the video streaming process.
        """
        if self.proc is None:
            return 'ERROR', 'Process not started'
        else:
            t = time.time()
            self.proc.terminate()
            status = self.proc.wait()
            self.proc = None
            return 'STOPPED', {'time': t, 'status': status}

    def do_LOOKAT(self, payload):
        """
        Adjust the camera orientation.
        """
        resp = self.orientation
        if payload:
            if self.monitor is None or self.monitor.ready():
                speed = payload.get('speed', 10)
                if speed > 0:
                    self.monitor = gevent.spawn(
                        self._ptmon,
                        payload.get('heading', resp['heading']),
                        payload.get('pitch', resp['pitch']),
                        speed,
                        self.async_cb)
                else:
                    return 'ERROR', 'Invalid speed specified'
            else:
                return 'ERROR', 'Adjustment in progress'
        return 'LOOKINGAT', resp

    def do_LIGHTS(self, payload):
        """
        Adjust the light intensity.
        """
        resp = {}
        try:
            if payload:
                self.lights.intensity = payload['intensity']
            resp['time'] = time.time()
            resp['intensity'] = self.lights.intensity
        except IOError:
            self.logger.exception('LIGHTS command failed')
            return 'ERROR', 'Light controller I/O error'
        else:
            return 'LIGHTS', resp

    def do_CAMERA(self, payload):
        """
        Adjust the camera settings.
        """
        if payload:
            resp = payload
        else:
            resp = {'zoom': 1, 'lasers': 'on'}
            resp['power'] = self.proc and 'on' or 'off'
        resp['time'] = time.time()
        return 'CAMERA', resp

    def do_ADREAD(self, payload):
        if payload:
            n = payload['average']
        else:
            n = 1
        data = []
        t = time.time()
        try:
            for name, value, units in HDCAM.readadc(n):
                data.append({
                    'name': name,
                    'val': value,
                    'units': units
                })
        except IOError:
            self.logger.exception('ADREAD command failed')
            return 'ERROR', 'Cannot read ADC'
        else:
            return 'ADVAL', {'time': t, 'data': data}


def publish(socket, msg):
    reply, contents = msg
    if contents:
        socket.send_multipart([reply, json.dumps(contents)])


def server(cfg, rep_endpoint, pub_endpoint):
    ctx = zmq.Context()
    socket = ctx.socket(zmq.REP)
    pub = ctx.socket(zmq.PUB)
    socket.bind(rep_endpoint)
    pub.bind(pub_endpoint)
    svc = Service(cfg, partial(publish, pub), nosensor=False)
    while True:
        command, payload = socket.recv_multipart()
        try:
            if payload:
                reply, contents = svc.dispatch(command, json.loads(payload))
            else:
                reply, contents = svc.dispatch(command, '')
        except Exception as e:
            logging.exception('Uncaught exception in %s', command)
            reply = 'ERROR'
            contents = str(e)
        if contents:
            socket.send_multipart([reply, json.dumps(contents)])
            pub.send_multipart([reply, json.dumps(contents)])
        else:
            socket.send_multipart([reply, ''])


def catch_signal(signum, frame):
    sys.exit(0)


if __name__ == '__main__':
    import os.path
    defcfg = os.path.join(sys.prefix,
                          'share',
                          'hdcam',
                          'hdcamctl.yaml')
    try:
        cfg = HDCAM.load_configuration(sys.argv[1])
    except IndexError:
        cfg = HDCAM.load_configuration(defcfg)
    logging.basicConfig(level=logging.DEBUG)
    signal.signal(signal.SIGTERM, catch_signal)
    try:
        server(cfg, 'tcp://*:5500', 'tcp://*:5501')
    finally:
        HDCAM.Relays._rb(0)
