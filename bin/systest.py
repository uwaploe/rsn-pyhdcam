#!/usr/bin/env python
#
# Test the HDCAM system.
#
import pyhdcam.system as HDCAM
import sys


def main():
    try:
        cfg = HDCAM.load_configuration(sys.argv[1])
    except IndexError:
        sys.stderr.write('Usage: systest.py cfgfile\n')
        sys.exit(1)

    try:
        print HDCAM.readadc()
        pt = HDCAM.PanTilt(cfg, nosensor=True)
        for t, status in pt.adjust(pan=180, tilt=180, speed=10):
            print t, status
    except KeyboardInterrupt:
        HDCAM.Relays._rb(0)


if __name__ == '__main__':
    main()
