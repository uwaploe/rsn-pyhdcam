#!/usr/bin/env python
#
# ZeroMQ based server for HDCAM (experimental)
#
import sys
import shlex
import gevent
from gevent_zeromq import zmq
import simplejson as json
import logging
import subprocess
import time
import signal
from functools import partial
import pyhdcam.system as HDCAM
from pyhdcam.camera import Commands

# Map input video resolution and frame-rate to mode-codes for the
# Ultragrid program
VMODES = {
    ('1080p', '29.97'): 6,
    ('1080p', '30'): 7,
    ('1080i', '59.94'): 9,
    ('1080i', '60'): 10,
    ('1080p', '59.94'): 12,
    ('1080p', '60'): 13,
    ('720p', '59.94'): 15,
    ('720p', '60'): 16
}

UV_TEMPLATE = 'uv -t decklink:0:{mode:d} -m {mtu:d} {endpoint}'


class InitService(object):
    """
    Class to implement the server side of the HDCAM Control Protocol. Each
    command, CMD, is handled by a method named 'do_CMD'.

    This class implements the initial state of the service. Once the START
    command is given, control passes to the :class:`Service` class.
    """
    defaults = {'vres': '1080i',
                'vrate': '59.94',
                'mtu': 8500,
                'endpoint': '',
                'logfile': '/dev/null'}
    workdir = '/var/tmp'

    def __init__(self, cfg, async_cb, **kwds):
        """
        :param cfg: configuration dictionary
        :param async_cb: callback for asynchronous operations
        """
        self.logger = logging.getLogger('server')
        self.async_cb = async_cb
        self.cfg = cfg
        self.proc = None
        self.svc = None

    def dispatch(self, command, payload):
        name = 'do_' + command.upper()
        f = getattr(self.svc, name, getattr(self, name, None))
        if f:
            self.logger.info('Executing %s', command)
            self.logger.debug('payload: %r', payload)
            return f(payload)
        return 'ERROR', 'Invalid command'

    def do_START(self, payload):
        """
        Start the video streaming server.
        """
        if self.proc is not None:
            return 'ERROR', 'Process is already running'

        if 'endpoint' not in payload:
            return 'ERROR', 'No endpoint specified'

        params = dict(
            endpoint=payload['endpoint'],
            mtu=payload.get('mtu',
                            self.defaults['mtu'])
        )
        vres = payload.get('vres', self.defaults['vres'])
        vrate = payload.get('vrate', self.defaults['vrate'])
        params['mode'] = VMODES.get((vres, vrate))
        if not params['mode']:
            return 'ERROR', 'Invalid video mode'
        t0 = time.time()
        try:
            self.logger.info('Initializing peripheral devices')
            lights = payload.get('lights', [])
            # If 'lights' is an empty string, turn it into
            # an empty list. If it's a string, turn it into
            # a single element list.
            if lights == '':
                lights = []
            elif not isinstance(lights, list):
                lights = [lights]
            self.svc = Service(self.cfg, self.async_cb,
                               nosensor=payload.get('nosensor', 0),
                               nocamera=payload.get('nocamera', 0),
                               nopt=payload.get('nopt', 0),
                               lights=lights)
        except Exception as e:
            self.logger.exception('System initialization failed')
            return 'ERROR', repr(e)
        self.logger.info('Startup time = %.3f s', (time.time() - t0))
        uv_cmd = UV_TEMPLATE.format(**params)
        self.logger.info('Starting video streaming: %r', uv_cmd)
        outf = open(payload.get('logfile', self.defaults['logfile']), 'w')
        try:
            t = time.time()
            self.proc = subprocess.Popen(
                shlex.split(uv_cmd),
                stdout=outf,
                stderr=subprocess.STDOUT,
                cwd=self.workdir
            )
        except (OSError, ValueError) as e:
            self.logger.exception('START command failed')
            return 'ERROR', str(e)
        rval = {'time': int(t),
                'cmd': uv_cmd,
                'pid': self.proc.pid}
        if self.svc.pt:
            rval['pt'] = True
            rval['sensor'] = self.svc.pt.sensor is not None
        return 'STARTED', rval

    def do_STOP(self, payload):
        """
        Stop the video streaming process. Also halts any in-progress position
        adjustments and powers-off all peripheral devices.
        """
        if self.proc is None:
            return 'ERROR', 'Process not started'
        else:
            t = time.time()
            self.proc.terminate()
            time.sleep(3)
            self.proc.kill()
            status = self.proc.wait()
            self.proc = None
            if self.svc.pt is not None:
                self.svc.pt.stop()
            del self.svc
            self.svc = None
            return 'STOPPED', {'time': int(t), 'status': status}

    def do_ADREAD(self, payload):
        if payload:
            n = payload['average']
        else:
            n = 1
        data = []
        t = time.time()
        try:
            for name, value, units in HDCAM.readadc(n):
                data.append({
                    'name': name,
                    'val': value,
                    'units': units
                })
        except IOError:
            self.logger.exception('ADREAD command failed')
            return 'ERROR', 'Cannot read ADC'
        else:
            return 'ADVAL', {'time': int(t), 'data': data}


class Service(object):
    """
    Class to implement the server side of the HDCAM Control Protocol. Each
    command, CMD, is handled by a method named 'do_CMD'.
    """
    def __init__(self, cfg, async_cb, **kwds):
        """
        :param cfg: configuration dictionary
        :param async_cb: callback for asynchronous operations
        """
        self.logger = logging.getLogger('server')
        if not kwds.get('nocamera', False):
            self.camera = HDCAM.VideoCam(cfg)
            # Slow down the zoom
            self.camera.send(Commands.ZOOM_SLOW, timeout=5)
        else:
            self.logger.info('Camera disabled')
            self.camera = None
        self.lights = HDCAM.Lights(cfg, names=kwds.get('lights'))
        if not kwds.get('nopt', False):
            self.pt = HDCAM.PanTilt(cfg, nosensor=kwds.get('nosensor', False))
        else:
            self.logger.info('Pan/tilt disabled')
            self.pt = None
        self.lights.flash(0.25, [10, 10])
        self.async_cb = async_cb
        self.cfg = cfg
        self.monitor = None
        self.zoom_level = 0

    @property
    def orientation(self):
        """
        Camera orientation.
        """
        fb = self.pt.feedback()
        return {
            'heading': fb.heading,
            'pitch': fb.pitch,
            'pan': fb.pan,
            'tilt': fb.tilt,
            'pan_flag': fb.pan_status,
            'tilt_flag': fb.tilt_status,
            'time': int(time.time()),
            'is_moving': (fb.pan_status & 1) | (fb.tilt_status & 1)
        }

    def _ptmon(self, pan_angle, tilt_angle, speed, cb):
        """
        Task to monitor the heading/pitch motion and pass the orientation
        information to a callback.
        """
        for t, status in self.pt.adjust(pan_angle=pan_angle, tilt_angle=tilt_angle,
                                        speed=speed):
            if isinstance(status, Exception):
                msg = 'ERROR'
                contents = str(status)
            else:
                contents = {'heading': status.heading,
                            'pitch': status.pitch,
                            'pan': status.pan,
                            'tilt': status.tilt,
                            'roll': status.roll,
                            'pan_flag': status.pan_status,
                            'tilt_flag': status.tilt_status,
                            'time': int(t)}
                if (status.pan_status & 1) or (status.tilt_status & 1):
                    msg = 'INMOTION'
                elif (status.pan_status & 2) or (status.tilt_status & 2):
                    msg = 'STALLED'
                else:
                    msg = 'STOPPED'
            cb((msg, contents))

    def do_LOOKAT(self, payload):
        """
        Adjust the camera orientation.
        """
        if self.pt is None:
            return 'ERROR', 'Pan/Tilt is not active'
        resp = self.orientation
        if payload:
            if self.monitor is None or self.monitor.ready():
                speed = payload.get('speed', 0)
                if speed >= 0:
                    self.monitor = gevent.spawn(
                        self._ptmon,
                        payload.get('pan', -1),
                        payload.get('tilt', -1),
                        speed,
                        self.async_cb)
                else:
                    return 'ERROR', 'Invalid speed specified'
            else:
                return 'ERROR', 'Adjustment in progress'
        return 'LOOKINGAT', resp

    def do_ADJUST(self, payload):
        if self.pt is None:
            return 'ERROR', 'Pan/Tilt is not active'
        if payload:
            if self.monitor is None or self.monitor.ready():
                where = self.orientation
                self.monitor = gevent.spawn(
                    self._ptmon,
                    payload.get('pan', 0) + where['pan'],
                    payload.get('tilt', 0) + where['tilt'],
                    payload.get('speed', 1),
                    self.async_cb)
                return 'LOOKINGAT', where
            else:
                return 'ERROR', 'Adjustment in progress'
        else:
            return 'ERROR', 'Missing arguments'

    def do_LIGHTS(self, payload):
        """
        Adjust the light intensity.
        """
        resp = {}
        try:
            if payload:
                self.lights.intensity = payload['intensity']
            resp['time'] = int(time.time())
            resp['intensity'] = self.lights.intensity
        except IOError:
            self.logger.exception('LIGHTS command failed')
            return 'ERROR', 'Light controller I/O error'
        else:
            return 'LIGHTS', resp

    def do_CAMERA(self, payload):
        """
        Adjust the camera settings.
        """
        if self.camera is None:
            return 'ERROR', 'Camera not available'
        # Convert various string boolean values to integers for laser setting
        states = {'on': 1, 'off': 0, '1': 1, '0': 0}
        laser_current = states[self.camera.get('laser', '0')]
        laser_next = laser_current
        resp = {}
        if payload:
            try:
                laser_next = states[str(payload['laser'])]
            except KeyError:
                laser_next = laser_current
            zoom = payload.get('zoom', 0)
            self.zoom_level += zoom
            while zoom > 0:
                self.camera.send(Commands.ZOOM_IN, timeout=5)
                zoom -= 1
            while zoom < 0:
                self.camera.send(Commands.ZOOM_OUT, timeout=5)
                zoom += 1
            if (laser_next ^ laser_current) == 1:
                r = self.camera.send(Commands.TOGGLE_LASER, timeout=5)
                _, tag, val = r.strip().split(':')
                if tag == 'Lasers':
                    self.camera['laser'] = val
        resp['zoom'] = self.zoom_level
        resp['laser'] = ('off', 'on')[laser_next]
        resp['time'] = int(time.time())
        return 'CAMERA', resp


def publish(socket, msg):
    reply, contents = msg
    if contents:
        socket.send_multipart([reply, json.dumps(contents)])


def server(cfg, rep_endpoint, pub_endpoint):
    ctx = zmq.Context()
    socket = ctx.socket(zmq.REP)
    pub = ctx.socket(zmq.PUB)
    socket.bind(rep_endpoint)
    pub.bind(pub_endpoint)
    logging.info('Camera server starting')
    svc = InitService(cfg, partial(publish, pub), nosensor=False)
    while True:
        command, payload = socket.recv_multipart()
        try:
            if payload:
                reply, contents = svc.dispatch(command, json.loads(payload))
            else:
                reply, contents = svc.dispatch(command, '')
        except Exception as e:
            logging.exception('Uncaught exception in %s', command)
            reply = 'ERROR'
            contents = str(e)
        if contents:
            socket.send_multipart([reply, json.dumps(contents)])
            pub.send_multipart([reply, json.dumps(contents)])
        else:
            socket.send_multipart([reply, ''])


def catch_signal(signum, frame):
    sys.exit(0)


if __name__ == '__main__':
    import os.path
    defcfg = os.path.join(sys.prefix,
                          'share',
                          'hdcam',
                          'hdcamctl.yaml')
    try:
        cfg = HDCAM.load_configuration(sys.argv[1])
    except IndexError:
        cfg = HDCAM.load_configuration(defcfg)
    logging.basicConfig(level=logging.DEBUG)
    signal.signal(signal.SIGTERM, catch_signal)
    try:
        server(cfg, 'tcp://*:5500', 'tcp://*:5501')
    finally:
        if cfg['camera'].get('force_off'):
            HDCAM.Relays._rb(0)
        else:
            # Leave camera powered on
            HDCAM.Relays._rb(0x01)
