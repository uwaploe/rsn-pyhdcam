#!/usr/bin/env python
#
"""%prog [command [command ...]]

Command interpreter interface to the ROS Pan/Tilt. Use the 'help' command
for more details:

    %prog help
"""
import pyhdcam.system as HDCAM
import cmd2
import sys
import os
from optparse import OptionParser
from ast import literal_eval


def args_to_dict(args):
    """
    Convert a command argument list of the form NAME=VALUE
    into a dictionary.

    >>> d = args_to_dict('foo=bar baz=1,2,3').items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar')]
    """
    def maybe_eval(s):
        try:
            return literal_eval(s)
        except ValueError:
            return s
    params = {}
    for name, val in [arg.split('=') for arg in args.split()]:
        if ',' in val:
            params[name] = [maybe_eval(v) for v in val.split(',')]
        else:
            params[name] = maybe_eval(val)
    return params


class PtApp(cmd2.Cmd):
    """
    Simple command interpreter for testing the Pan/Tilt

    Commands (type help <topic>)
    ===============================================
    moveto goto where limits quit
    """
    prompt = 'PT> '
    pt = None

    def do_where(self, args):
        """where: show current rotator positions"""
        assert self.pt is not None
        fb = self.pt.feedback()
        self.poutput('pan={0:.1f} tilt={1:.1f}\n'.format(fb.pan, fb.tilt))

    def do_moveto(self, args):
        """moveto [pan=DEG [tilt=DEG]]: move to a relative position"""
        assert self.pt is not None
        params = args_to_dict(args)
        fb = self.pt.feedback()
        pan = params.get('pan', 0) + fb.pan
        tilt = params.get('tilt', 0) + fb.tilt
        speed = params.get('speed', 0)
        for t, status in self.pt.adjust(pan_angle=pan,
                                        tilt_angle=tilt, speed=speed):
            if isinstance(status, Exception):
                self.perror(str(status) + '\n')
                break
            else:
                self.poutput('pan={0:.1f} tilt={1:.1f}\n'.format(
                    status.pan, status.tilt))

    def do_goto(self, args):
        """goto [pan=DEG [tilt=DEG]]: move to an absolute position"""
        assert self.pt is not None
        params = args_to_dict(args)
        pan = params.get('pan', -1)
        tilt = params.get('tilt', -1)
        speed = params.get('speed', 0)
        for t, status in self.pt.adjust(pan_angle=pan,
                                        tilt_angle=tilt, speed=speed):
            if isinstance(status, Exception):
                self.perror(str(status) + '\n')
                break
            else:
                self.poutput('pan={0:.1f} tilt={1:.1f}\n'.format(
                    status.pan, status.tilt))

    def do_limits(self, args):
        """limits [pan.ccw=DEG] [pan.cw=DEG] [tilt.ccw=DEG] [tilt.cw=DEG]"""
        assert self.pt is not None
        params = args_to_dict(args)
        for k, v in params.items():
            r, lim = k.split('.')
            if r == 'pan':
                setattr(self.pt.pan, lim, v)
            elif r == 'tilt':
                setattr(self.pt.tilt, lim, v)
        self.poutput('pan.ccw={0:.0f} '.format(self.pt.pan.ccw))
        self.poutput('pan.cw={0:.0f} '.format(self.pt.pan.cw))
        self.poutput('tilt.ccw={0:.0f} '.format(self.pt.tilt.ccw))
        self.poutput('tilt.cw={0:.0f}\n'.format(self.pt.tilt.cw))

    def do_help(self, arg):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            self.poutput(self.__doc__ + '\n')

    def emptyline(self):
        pass


def main():
    parser = OptionParser(usage=__doc__)
    parser.parse_args()
    obj = PtApp()
    cfgfile = os.path.join(sys.prefix,
                           'share',
                           'hdcam',
                           'hdcamctl.yaml')
    cfg = HDCAM.load_configuration(cfgfile)
    obj.pt = HDCAM.PanTilt(cfg, nosensor=True)
    try:
        obj.cmdloop()
    finally:
        if obj.pt:
            obj.pt.stop()
        del obj.pt


if __name__ == '__main__':
    main()
