#!/usr/bin/env python
#
# Simulate the HDCAM server
#
import sys
import shlex
import gevent
from gevent_zeromq import zmq
import simplejson as json
import logging
import subprocess
import time
import signal
import math
from functools import partial
from mock import Mock
from decimal import Decimal
from collections import namedtuple
from gevent.queue import Queue


# Map input video resolution and frame-rate to mode-codes for the
# Ultragrid program
VMODES = {
    ('1080p', '29.97'): 6,
    ('1080p', '30'): 7,
    ('1080i', '59.94'): 9,
    ('1080i', '60'): 10,
    ('1080p', '59.94'): 12,
    ('1080p', '60'): 13,
    ('720p', '59.94'): 15,
    ('720p', '60'): 16
}

UV_TEMPLATE = '/usr/bin/yes'


class InitService(object):
    """
    Class to implement the server side of the HDCAM Control Protocol. Each
    command, CMD, is handled by a method named 'do_CMD'.

    This class implements the initial state of the service. Once the START
    command is given, control passes to the :class:`Service` class.
    """
    defaults = {'vres': '1080i',
                'vrate': '59.94',
                'mtu': 8500,
                'endpoint': '',
                'logfile': '/dev/null'}
    workdir = '/var/tmp'

    def __init__(self, cfg, async_cb, **kwds):
        """
        :param cfg: configuration dictionary
        :param async_cb: callback for asynchronous operations
        """
        self.logger = logging.getLogger('server')
        self.async_cb = async_cb
        self.cfg = cfg
        self.proc = None
        self.svc = None

    def dispatch(self, command, payload):
        name = 'do_' + command.upper()
        f = getattr(self.svc, name, getattr(self, name, None))
        if f:
            self.logger.info('Executing %s', command)
            self.logger.debug('payload: %r', payload)
            return f(payload)
        return 'ERROR', 'Invalid command'

    def do_START(self, payload):
        """
        Start the video streaming server.
        """
        if self.proc is not None:
            return 'ERROR', 'Process is already running'

        if 'endpoint' not in payload:
            return 'ERROR', 'No endpoint specified'

        params = dict(
            endpoint=payload['endpoint'],
            mtu=payload.get('mtu',
                            self.defaults['mtu'])
        )
        vres = payload.get('vres', self.defaults['vres'])
        vrate = payload.get('vrate', self.defaults['vrate'])
        params['mode'] = VMODES.get((vres, vrate))
        if not params['mode']:
            return 'ERROR', 'Invalid video mode'
        t0 = time.time()
        try:
            self.logger.info('Initializing peripheral devices')
            lights = payload.get('lights', [])
            # If 'lights' is an empty string, turn it into
            # an empty list. If it's a string, turn it into
            # a single element list.
            if lights == '':
                lights = []
            elif not isinstance(lights, list):
                lights = [lights]
            self.svc = Service(self.cfg, self.async_cb,
                               nosensor=payload.get('nosensor', 0),
                               nocamera=payload.get('nocamera', 0),
                               lights=lights)
        except Exception as e:
            self.logger.exception('System initialization failed')
            return 'ERROR', repr(e)
        self.logger.info('Startup time = %.3f s', (time.time() - t0))
        uv_cmd = UV_TEMPLATE.format(**params)
        self.logger.info('Starting video streaming: %r', uv_cmd)
        outf = open(payload.get('logfile', self.defaults['logfile']), 'w')
        try:
            t = time.time()
            self.proc = subprocess.Popen(
                shlex.split(uv_cmd),
                stdout=outf,
                stderr=subprocess.STDOUT,
                cwd=self.workdir
            )
        except (OSError, ValueError) as e:
            self.logger.exception('START command failed')
            return 'ERROR', str(e)
        return 'STARTED', {'time': t, 'cmd': uv_cmd, 'pid': self.proc.pid}

    def do_STOP(self, payload):
        """
        Stop the video streaming process. Also halts any in-progress position
        adjustments and powers-off all peripheral devices.
        """
        if self.proc is None:
            return 'ERROR', 'Process not started'
        else:
            t = time.time()
            self.proc.terminate()
            status = self.proc.wait()
            self.proc = None
            self.svc.pt.stop()
            del self.svc
            self.svc = None
            return 'STOPPED', {'time': int(t), 'status': status}

    def do_ADREAD(self, payload):
        fakeadc = [
            ('temp_high_watt', Decimal('21.2'), 'degC'),
            ('temp_low_watt', Decimal('10.1'), 'degC'),
            ('temp_relay', Decimal('12.5'), 'degC')
        ]

        if payload:
            n = payload['average']
        else:
            n = 1
        data = []
        t = time.time()
        try:
            for name, value, units in fakeadc:
                data.append({
                    'name': name,
                    'val': value,
                    'units': units
                })
        except IOError:
            self.logger.exception('ADREAD command failed')
            return 'ERROR', 'Cannot read ADC'
        else:
            return 'ADVAL', {'time': int(t), 'data': data}


Feedback = namedtuple('Feedback',
                      'pan tilt heading pitch roll pan_status tilt_status')


def motion(x0, vmax, accel):
    def f():
        t = time.time()
        dt = t - f.last_t
        dv = accel * dt
        dx = min((f.last_v + dv/2), vmax) * dt
        x = f.last_x + dx
        f.last_v = min(f.last_v + dv, vmax)
        f.last_x = x
        f.last_t = t
        return x
    f.last_t = time.time()
    f.last_x = x0
    f.last_v = 0
    return f


class PtSim(object):
    pan = 180
    tilt = 90
    heading = 0
    pitch = 0
    pan_status = 0
    tilt_status = 0
    pan_speed = 10
    tilt_speed = 10
    accel = 2
    task = None
    tilt_range = [50, 140]
    pan_range = [45, 315]

    def feedback(self):
        return Feedback(pan=self.pan,
                        tilt=self.tilt,
                        heading=self.heading,
                        pitch=self.pitch,
                        roll=0,
                        pan_status=self.pan_status,
                        tilt_status=self.tilt_status)

    def _worker(self, pan, tilt, dataq, interval):
        d_pan = pan - self.pan
        d_tilt = tilt - self.tilt
        f_pan = motion(self.pan,
                       math.copysign(self.pan_speed, d_pan), 5)
        f_tilt = motion(self.tilt,
                        math.copysign(self.tilt_speed, d_tilt), 5)

        while pan != self.pan or tilt != self.tilt:
            d_pan = pan - self.pan
            d_tilt = tilt - self.tilt
            if abs(d_pan) > abs(f_pan.last_v):
                self.pan = f_pan()
                self.pan_status = 1
            else:
                self.pan_status = 0
                self.pan = pan

            if abs(d_tilt) > abs(f_tilt.last_v):
                self.tilt = f_tilt()
                self.tilt_status = 1
            else:
                self.tilt_status = 0
                self.tilt = tilt

            dataq.put((time.time(), self.feedback()))
            gevent.sleep(interval)

        self.pan_status = 0
        self.tilt_status = 0
        dataq.put((time.time(), self.feedback()))
        dataq.put(StopIteration)

    def adjust(self, pan_angle=None, tilt_angle=None, speed=10):
        dataq = Queue()
        pan, tilt = self.pan, self.tilt
        if pan_angle is not None:
            pan = min(max(pan_angle, self.pan_range[0]),
                      self.pan_range[1])
            if speed > 0:
                self.pan_speed = speed
        if tilt_angle is not None:
            tilt = min(max(tilt_angle, self.tilt_range[0]),
                       self.tilt_range[1])
            if speed > 0:
                self.tilt_speed = speed
        self.task = gevent.spawn(self._worker, pan, tilt, dataq, 1)
        return dataq

    def stop(self):
        """
        Stop any in-progress position adjustment.
        """
        if self.task and not self.task.ready():
            self.task.kill(timeout=3)


class LightSim(object):
    intensity = [0, 0]

    def flash(self, *args):
        pass


class Service(object):
    """
    Class to implement the server side of the HDCAM Control Protocol. Each
    command, CMD, is handled by a method named 'do_CMD'.
    """
    def __init__(self, cfg, async_cb, **kwds):
        """
        :param cfg: configuration dictionary
        :param async_cb: callback for asynchronous operations
        """
        self.logger = logging.getLogger('server')
        if not kwds.get('nocamera', False):
            self.camera = Mock()
            self.camera.laser = 'off'
            self.camera.zoom = 0
        else:
            self.logger.info('Camera disabled')
            self.camera = None
        self.lights = LightSim()
        self.pt = PtSim()
        self.lights.flash(0.25, [10, 10])
        self.async_cb = async_cb
        self.cfg = cfg
        self.monitor = None
        self.zoom_level = 0

    @property
    def orientation(self):
        """
        Camera orientation.
        """
        fb = self.pt.feedback()
        return {
            'heading': fb.heading,
            'pitch': fb.pitch,
            'pan': fb.pan,
            'tilt': fb.tilt,
            'pan_flag': fb.pan_status,
            'tilt_flag': fb.tilt_status,
            'time': int(time.time()),
            'is_moving': (fb.pan_status & 1) | (fb.tilt_status & 1)
        }

    def _ptmon(self, pan_angle, tilt_angle, speed, cb):
        """
        Task to monitor the heading/pitch motion and pass the orientation
        information to a callback.
        """
        for t, status in self.pt.adjust(pan_angle=pan_angle,
                                        tilt_angle=tilt_angle,
                                        speed=speed):
            if isinstance(status, Exception):
                msg = 'ERROR'
                contents = str(status)
            else:
                contents = {'heading': status.heading,
                            'pitch': status.pitch,
                            'pan': status.pan,
                            'tilt': status.tilt,
                            'roll': status.roll,
                            'pan_flag': status.pan_status,
                            'tilt_flag': status.tilt_status,
                            'time': int(t)}
                if (status.pan_status & 1) or (status.tilt_status & 1):
                    msg = 'INMOTION'
                elif (status.pan_status & 2) or (status.tilt_status & 2):
                    msg = 'STALLED'
                else:
                    msg = 'STOPPED'
            cb((msg, contents))

    def do_LOOKAT(self, payload):
        """
        Adjust the camera orientation.
        """
        resp = self.orientation
        if payload:
            if self.monitor is None or self.monitor.ready():
                speed = payload.get('speed', 0)
                if speed >= 0:
                    self.monitor = gevent.spawn(
                        self._ptmon,
                        payload.get('pan'),
                        payload.get('tilt'),
                        speed,
                        self.async_cb)
                else:
                    return 'ERROR', 'Invalid speed specified'
            else:
                return 'ERROR', 'Adjustment in progress'
        return 'LOOKINGAT', resp

    def do_ADJUST(self, payload):
        if payload:
            if self.monitor is None or self.monitor.ready():
                where = self.orientation
                self.monitor = gevent.spawn(
                    self._ptmon,
                    payload.get('pan', 0) + where['pan'],
                    payload.get('tilt', 0) + where['tilt'],
                    payload.get('speed', 1),
                    self.async_cb)
                return 'LOOKINGAT', where
            else:
                return 'ERROR', 'Adjustment in progress'
        else:
            return 'ERROR', 'Missing arguments'

    def do_LIGHTS(self, payload):
        """
        Adjust the light intensity.
        """
        resp = {}
        try:
            if payload:
                self.lights.intensity = payload['intensity']
            resp['time'] = int(time.time())
            resp['intensity'] = self.lights.intensity
        except IOError:
            self.logger.exception('LIGHTS command failed')
            return 'ERROR', 'Light controller I/O error'
        else:
            return 'LIGHTS', resp

    def do_CAMERA(self, payload):
        """
        Adjust the camera settings.
        """
        if self.camera is None:
            return 'ERROR', 'Camera not available'
        # Convert various string boolean values to integers for laser setting
        states = {'on': 1, 'off': 0, '1': 1, '0': 0}
        laser_current = states[self.camera.laser]
        laser_next = laser_current
        resp = {}
        if payload:
            try:
                laser_next = states[str(payload['laser'])]
            except KeyError:
                laser_next = laser_current
            zoom = payload.get('zoom', 0)
            self.zoom_level += zoom
            while zoom > 0:
                gevent.sleep(3)
                zoom -= 1
            while zoom < 0:
                gevent.sleep(3)
                zoom += 1
            if (laser_next ^ laser_current) == 1:
                gevent.sleep(3)
        resp['zoom'] = self.zoom_level
        resp['laser'] = ('off', 'on')[laser_next]
        resp['time'] = int(time.time())
        self.camera.laser = resp['laser']
        self.camera.zoom = resp['zoom']
        return 'CAMERA', resp


def publish(socket, msg):
    reply, contents = msg
    if contents:
        socket.send_multipart([reply, json.dumps(contents)])


def server(cfg, rep_endpoint, pub_endpoint):
    ctx = zmq.Context()
    socket = ctx.socket(zmq.REP)
    pub = ctx.socket(zmq.PUB)
    socket.bind(rep_endpoint)
    pub.bind(pub_endpoint)
    logging.info('Camera server starting')
    svc = InitService(cfg, partial(publish, pub), nosensor=False)
    while True:
        command, payload = socket.recv_multipart()
        try:
            if payload:
                reply, contents = svc.dispatch(command, json.loads(payload))
            else:
                reply, contents = svc.dispatch(command, '')
        except Exception as e:
            logging.exception('Uncaught exception in %s', command)
            reply = 'ERROR'
            contents = str(e)
        if contents:
            socket.send_multipart([reply, json.dumps(contents)])
            pub.send_multipart([reply, json.dumps(contents)])
        else:
            socket.send_multipart([reply, ''])


def catch_signal(signum, frame):
    sys.exit(0)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    signal.signal(signal.SIGTERM, catch_signal)
    cfg = {}
    server(cfg, 'tcp://*:5500', 'tcp://*:5501')
