#!/usr/bin/env python
#
# Test program for the NCD Relay Board used on the RSN HD Camera System. The board
# has a USB/serial interface which is accessed through ``/dev/ttyUSB0``.
#
"""%prog [command [command ...]]

Simple command interpreter interface to the NCD Relay Board. Use the 'help' command
for more details:

    %prog help
"""
from pyhdcam.serialcom import Port
from pyhdcam.ncd import RelayBank, read_adc10
from pyhdcam.utils import Calibration
import cmd2
from decimal import Decimal, getcontext
from optparse import OptionParser


class NcdTest(cmd2.Cmd):
    """
    Simple command-interpreter for testing an NCD relay board.

    Commands (type help <topic>)
    ===================================================
    set clear read adc load save
    """
    prompt = 'NCD> '
    #: Serial device
    device = '/dev/ttyUSB0'
    #: Serial baud rate
    baud = 115200
    #: A/D scaling factor, volts/count
    vscale = Decimal(5)/Decimal(1023)
    #: Precision of A/D voltage value
    exp = Decimal('0.001')
    port = None
    rawmode = False
    cals = {
        2: Calibration([
            Decimal('129.386'), Decimal('-114.921'),
            Decimal('59.1018'), Decimal('-15.6427'),
            Decimal('1.50251')
        ], 'degC'),
        3: Calibration([
            Decimal('129.386'), Decimal('-114.921'),
            Decimal('59.1018'), Decimal('-15.6427'),
            Decimal('1.50251')
        ], 'degC'),
        7: Calibration([
            Decimal('-20.51'), Decimal('51.28')
        ], 'degC'),
        0: Calibration([
            Decimal('0'), Decimal('85')
        ], 'volts'),
        1: Calibration([
            Decimal('0'), Decimal('0.555556')
        ], 'amps')
    }

    def _to_volts(self, x):
        return (Decimal(x)*self.vscale).quantize(self.exp)

    def precmd(self, line):
        if self.port is None:
            self.port = Port(self.device, baud=self.baud)
            self.port.open()
            self.rb = RelayBank(self.port)
            if not self.rb.is_alive():
                raise IOError('Cannot access NCD Relay board')
        return line

    def postloop(self):
        if self.port:
            self.port.close()

    def do_help(self, arg):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            self.poutput(self.__doc__+'\n')

    def do_set(self, rest):
        """set N: assert relay N (0-7)"""
        if rest:
            n = int(rest, 0)
            if 0 <= n < 8:
                self.rb[n] = 1
            else:
                self.perror('Invalid relay number\n')

    def do_read(self, rest):
        """read [N]: return the state of relay N or all relays"""
        states = ['off', 'on']
        if rest:
            n = int(rest, 0)
            if 0 <= n < 8:
                self.poutput(states[self.rb[n]] + '\n')
            else:
                self.perror('Invalid relay number\n')
        else:
            vals = [states[s] for s in self.rb[:]]
            self.poutput(
                '\n'.join(['%d: %s' % (i, v) for i, v in enumerate(vals)])+'\n')

    def do_clear(self, rest):
        """clear N: de-assert relay N (0-7)"""
        if rest:
            n = int(rest, 0)
            if 0 <= n < 8:
                self.rb[n] = 0
            else:
                self.perror('Invalid relay number\n')

    def do_rawmode(self, rest):
        """rawmode on|off: enable or disable raw A/D display"""
        if str(rest) in ('on', '1', 'ON'):
            self.rawmode = True
            self.pfeedback('Raw A/D display enabled')
        else:
            self.rawmode = False
            self.pfeedback('Raw A/D display disabled')

    def _convert_adc(self, x, channel):
        if (not self.rawmode) and channel in self.cals:
            y, units = self.cals[channel](self._to_volts(x))
            return channel, y, units
        else:
            return channel, self._to_volts(x), 'volts'

    def do_adc(self, rest, opts=None):
        """adc [N]: read A/D channel N or all channels."""
        vals = read_adc10(self.port)
        getcontext().prec = 4
        if rest:
            idx = int(rest, 0)
            if 0 <= idx < 8:
                self.poutput('%d: %s %s\n' % self._convert_adc(vals[idx], idx))
        else:
            self.poutput('\n'.join(
                ['%d: %s %s' % self._convert_adc(v, i)
                    for i, v in enumerate(vals)]
            ) + '\n')

    def do_quit(self, rest):
        """quit: exit the program"""
        return True

    do_eof = do_quit
    do_exit = do_quit
    do_q = do_quit

    def emptyline(self):
        pass


def main():
    parser = OptionParser(usage=__doc__)
    opts, args = parser.parse_args()
    obj = NcdTest()
    obj.cmdloop()

if __name__ == '__main__':
    main()
