#!/usr/bin/env python
#
# Stress test for NCD Relay Board
#
from pyhdcam.serialcom import Port
from pyhdcam.ncd import RelayBank, read_adc10
from pyhdcam.utils import Calibration
from decimal import Decimal, getcontext
import time
import itertools
import sys
import signal
from contextlib import contextmanager


#: A/D scaling factor, volts/count
VSCALE = Decimal(5) / Decimal(1023)
#: Relay temperature sensor calibration
CAL = Calibration([Decimal('-20.51'), Decimal('51.28')], 'degC')
#: A/D precision
EXP = Decimal('0.001')
#: A/D channel
ADCHAN = 7
#: Cycle of relay state changes
STATES = [
    0x03, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0x00, 0xff, 0xfe, 0xfc, 0xf8, 0xf0, 0xe0, 0xc0
]


@contextmanager
def disable_ints():
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    try:
        yield
    finally:
        signal.signal(signal.SIGINT, signal.default_int_handler)


def change_state(port, rb, state, t_hold=60):
    """
    Change the state of the relays and return the
    current board temperature.
    """
    with disable_ints():
        rb(state)
    time.sleep(t_hold)
    vals = [0] * 8
    with disable_ints():
        vals = read_adc10(port) or read_adc10(port)
    if vals:
        v = (Decimal(vals[ADCHAN]) * VSCALE).quantize(EXP)
        return CAL(v)
    else:
        return None, None


def timestamp():
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())


def main():
    port = Port('/dev/ttyUSB0', baud=115200)
    port.open()
    rb = RelayBank(port)
    if not rb.is_alive():
        raise IOError('Cannot access NCD Relay board')
    getcontext().prec = 4
    try:
        for state in itertools.cycle(STATES):
            sys.stdout.write('[{0}] state={1:02x}\n'.format(timestamp(),
                                                            state))
            try:
                temp, units = change_state(port, rb, state)
            except IOError:
                sys.stderr.write('[{0}] A/D read error\n'.format(timestamp))
            else:
                sys.stdout.write('[{0}] temperature={1}\n'.format(timestamp(),
                                                                  temp))
    except KeyboardInterrupt:
        sys.stderr.write('Exiting. Switching off all relays\n')
        # Turn all relays off
        rb(0)
    port.close()


if __name__ == '__main__':
    main()
