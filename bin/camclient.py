#!/usr/bin/env python
#
"""%prog [options] [command [command ...]]

Command interpreter interface to the HDCAM server. Use the 'help' command
for more details:

    %prog help
"""
from __future__ import print_function
import sys
import zmq
import simplejson as json
import shlex
import cmd2
import subprocess
import time
import fcntl
import os
import smtplib
import traceback
from email.MIMEText import MIMEText
from optparse import OptionParser
from ast import literal_eval
from pyhdcam.hyperdeck import Hyperdeck
from contextlib import contextmanager


UV_TEMPLATE = 'uv -d decklink:0 -m {mtu:d} {endpoint}'


def args_to_dict(args, do_eval=True):
    """
    Convert a command argument list of the form NAME=VALUE
    into a dictionary.

    >>> d = args_to_dict('foo=bar baz=1,2,3 x= y').items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar'), ('x', ''), ('y', None)]
    """
    def maybe_eval(s):
        if do_eval:
            try:
                return literal_eval(s)
            except (ValueError, SyntaxError):
                return s
        else:
            return s
    params = {}
    for arg in [a.split('=') for a in args.split()]:
        try:
            name, val = arg
        except ValueError:
            name = arg[0]
            val = None
        if (val is not None) and (',' in val):
            params[name] = [maybe_eval(v) for v in val.split(',')]
        else:
            params[name] = maybe_eval(val)
    return params


def send_msg(sock, msg, timeout=2500):
    """
    Send a multi-part message to a ZeroMQ endpoint and return the response.

    :param sock: socket object
    :type sock: zmq.core.socket.Socket
    :param msg: message contents
    :param timeout: maximum time (ms) to wait for a response
    :return: multi-part response
    :rtype: tuple or ``None``
    """
    result = None
    poll = zmq.Poller()
    poll.register(sock)
    sock.send_multipart(msg)
    socks = dict(poll.poll(timeout))
    if socks.get(sock) == zmq.POLLIN:
        result = sock.recv_multipart()
    poll.unregister(sock)
    return result


def subscription(sock, sentinel, timeout=10000):
    """
    Generator to return messages from a SUB socket until *sentinel*
    is seen or *timeout* expires.
    """
    poll = zmq.Poller()
    poll.register(sock)
    socks = dict(poll.poll(timeout))
    while socks.get(sock) == zmq.POLLIN:
        result = sock.recv_multipart()
        yield result
        if result[0] not in sentinel:
            socks = dict(poll.poll(timeout))
        else:
            break


class HdcamApp(cmd2.Cmd):
    """
    Simple command interpreter for accessing the CAMHD server

    Commands (type help <topic>)
    =================================================
    open close start stop lookat lights camera adread
    hd time twait sleep quit
    """
    std_prompt = 'HDCAM> '
    rec_prompt = '(rec)HDCAM> '
    socket = None
    ctx = None
    host = None
    proc = None
    timeout = 30
    epoch = 0
    recmode = False
    hd = None
    logf = None
    # Map the file format names used by the Hyperdeck
    # to something easier to remember and type.
    hd_formats = {
        'raw': 'QuickTimeUncompressed',
        'hq': 'QuickTimeProResHq'
    }

    def log_event(self, event, data=''):
        if self.logf:
            t = time.time()
            secs, usecs = divmod(int(t * 1000000), 1000000)
            self.logf.write('{0},{1},{2},"{3}"\n'.format(secs, usecs,
                                                         event, data))
            self.logf.flush()

    def _log_trace(self, line):
        self.log_event('command', line.strip())
        return line

    def _pass_cmd(self, line):
        return line
    precmd = _pass_cmd

    def preloop(self):
        self.prompt = self.std_prompt

    def _open(self, host):
        if self.socket is not None:
            self._close()
        if self.ctx is None:
            self.ctx = zmq.Context()
        self.host = host
        self.socket = self.ctx.socket(zmq.REQ)
        self.socket.connect('tcp://{0}:5500'.format(host))

    def _close(self):
        if self.proc is not None:
            self.perror('Stopping stream\n')
            self.do_stop('')
        self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.close()
        self.socket = None

    def _reopen(self):
        assert self.host is not None
        self._close()
        self._open(self.host)

    def _send(self, msg):
        result = send_msg(self.socket, msg, self.timeout * 1000)
        if result is None:
            self.perror('No response from server. Reconnecting ...\n')
            self._reopen()
            result = None, None
            if self.proc:
                self.pfeedback('Stopping Ultragrid process')
                self.proc.terminate()
                time.sleep(2)
                if self.proc.poll():
                    self.proc.kill()
                self.proc = None
        return result

    def do_help(self, arg):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            self.poutput(self.__doc__ + '\n')

    def do_open(self, args):
        """open HOST: open a connection to the CAMHD host."""
        if not args:
            self.perror('You must specify a hostname\n')
        else:
            self._open(args)

    def do_close(self, args):
        """close: close the network connection"""
        assert self.socket is not None
        self._close()

    def do_sleep(self, args):
        """sleep SECS: pause for SECS seconds."""
        if args:
            time.sleep(float(args))
        else:
            self.perror('missing argument')

    def do_twait(self, args):
        """twait [HH:[MM:[SS]]]: pause until absolute time since stream start."""
        if args:
            t = [int(a, 10) for a in args.split(':')]
            secs = reduce(lambda x, y: x * 60 + y, t, 0)
            try:
                time.sleep(self.epoch + secs - time.time())
            except IOError:
                self.perror('Warning: wait time is in the past\n')
        else:
            self.perror('missing argument')

    def do_start(self, args):
        """start endpoint=HOST [PARAM=VALUE ...]: start video streaming."""
        assert self.socket is not None
        if self.proc is not None:
            self.do_stop()

        params = args_to_dict(args, do_eval=False)
        resp, contents = self._send([
            'START',
            json.dumps(params)
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))
        if resp is None:
            self.perror("Invalid response from server, aborting\n")
            raise SystemExit("Invalid server response")

        if resp != 'ERROR':
            self.pfeedback('Starting local Ultragrid process')
            uv_cmd = UV_TEMPLATE.format(endpoint=self.host,
                                        mtu=int(params.get('mtu', 8500)))
            outf = open('/dev/null', 'w')
            try:
                self.proc = subprocess.Popen(
                    shlex.split(uv_cmd),
                    stdout=outf,
                    stderr=subprocess.STDOUT
                )
            except (OSError, ValueError) as e:
                self.perror(str(e) + '\n')
            else:
                self.epoch = time.time()

    def do_time(self, args):
        """time: display elapsed time since stream start."""
        if self.epoch == 0:
            self.poutput('0\n')
        else:
            dt = int(time.time() - self.epoch)
            h, rem = divmod(dt, 3600)
            m, s = divmod(rem, 60)
            self.poutput('{0:02d}:{1:02d}:{2:02d}\n'.format(h, m, s))

    def do_stop(self, args):
        """stop: stop video streaming."""
        assert self.socket is not None
        resp, contents = self._send([
            'STOP',
            b''
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))
        if self.proc:
            self.proc.terminate()
            self.poutput('Waiting for uv process to exit...\n')
            time.sleep(3)
            if self.proc.poll():
                self.proc.kill()
            self.proc = None
        self.epoch = 0

    def do_lookat(self, args):
        """lookat [pan=ANGLE tilt=ANGLE speed=DEG/S]: get or set absolute pan/tilt."""
        assert self.socket is not None
        params = args_to_dict(args)
        resp, contents = self._send([
            'LOOKAT',
            json.dumps(params)
        ])
        if args:
            sub = self.ctx.socket(zmq.SUB)
            sub.setsockopt(zmq.SUBSCRIBE, 'INMOTION')
            sub.setsockopt(zmq.SUBSCRIBE, 'STOPPED')
            sub.setsockopt(zmq.SUBSCRIBE, 'STALLED')
            sub.connect('tcp://{0}:5501'.format(self.host))
            for msg in subscription(sub, ('STOPPED', 'STALLED')):
                resp, contents = msg
                self.log_event('state', contents)
                self.poutput('{0}: {1}\n'.format(
                    resp,
                    contents
                ))
            sub.close()
        obj = json.loads(contents)
        if resp == 'ERROR':
            self.perror('ERROR: {0}\n'.format(contents))
        else:
            keys = ('pan', 'tilt', 'heading', 'pitch', 'pan_flag', 'tilt_flag')
            for k in keys:
                v = obj.get(k)
                if v is not None:
                    self.poutput('{0}: {1}\n'.format(k, v))

    def do_lights(self, args):
        """lights [INTENSITY1 [INTENSITY2]]: check or adjust light intensity."""
        assert self.socket is not None
        if args:
            vals = [int(a, 0) for a in args.split()]
            if len(vals) == 1:
                vals.append(vals[0])
            params = {'intensity': vals}
        else:
            params = {}
        resp, contents = self._send([
            'LIGHTS',
            json.dumps(params)
        ])
        self.poutput('{0}: {1}\n'.format(
            resp,
            contents
        ))

    def do_camera(self, args):
        """camera [laser=on|off zoom=STEPS]: check or adjust camera settings."""
        assert self.socket is not None
        params = args_to_dict(args)
        resp, contents = self._send([
            'CAMERA',
            json.dumps(params)
        ])
        obj = json.loads(contents)
        if resp == 'ERROR':
            self.perror('ERROR: {1}\n'.format(contents))
        else:
            for k, v in obj.iteritems():
                self.poutput('{0}: {1}\n'.format(k, v))

    def do_adread(self, args):
        """adread: read A/D channels."""
        assert self.socket is not None
        params = args_to_dict(args)
        resp, contents = self._send([
            'ADREAD',
            json.dumps(params)
        ])
        self.log_event('adc', contents)
        obj = json.loads(contents)
        if resp == 'ERROR':
            self.perror('ERROR: {1}\n'.format(obj))
        else:
            for row in obj['data']:
                self.poutput('{name}: {val:.3f} {units}\n'.format(**row))

    def help_hd(self):
        self.poutput("""
        hd open host=ADDR
          Initialize the connection to the Hyperdeck. This command
          must be sent before any subsequent 'hd' commands. The
          Hyperdeck hostname or IP must be supplied.

        hd rec [input=SDI] [fmt=QuickTimeUncompressed]
          Start recording a video clip.

        hd stop
          Stop the current recording.

        hd close
          Close the network connection to the Hyperdeck.
        """)

    def do_hd(self, args):
        params = args_to_dict(args)
        if 'open' in params:
            host = params.get('host')
            if not host:
                self.perror('ERROR: bad/missing host name\n')
            else:
                try:
                    self.hd = Hyperdeck(host)
                    self.hd.send_command('remote', enable='true')
                    self.hd.get_response()
                except Exception as e:
                    self.perror('ERROR: {0!r}\n'.format(e))
        else:
            if self.hd is None:
                self.perror('ERROR: run "hd open host=NAME" first\n')
                return
            if 'rec' in params:
                self.hd_configure(**params)
                self.start_recording()
            elif 'stop' in params:
                self.stop_recording()
            elif 'close' in params:
                self.stop_recording()
                self.hd = None
            else:
                self.poutput('Invalid argument: {0!r}\n'.format(args))

    def hd_configure(self, **kwds):
        """Change the Hyperdeck configuration."""
        if 'input' in kwds:
            self.hd.send_command('configuration',
                                 video_input=kwds['input'].upper())
            self.hd.get_response()
        if 'fmt' in kwds:
            fmt = self.hd_formats.get(kwds['fmt'], kwds['fmt'])
            self.hd.send_command('configuration',
                                 file_format=fmt)
            self.hd.get_response()

    def start_recording(self):
        """Start recording a video clip on the Hyperdeck"""
        if self.recmode:
            self.stop_recording()
        t = time.strftime('%Y%m%d_%H%M%S', time.gmtime())
        clip = 'clip_{0}_'.format(t)
        try:
            self.hd.send_command('configuration')
            code, resp = self.hd.get_response()
            self.poutput(resp)
            self.hd.send_command('record', name=clip)
            code, resp = self.hd.get_response()
            self.poutput(resp)
            filename = 'log_{0}.csv'.format(t)
            self.logf = open(filename, 'w')
            self.poutput('Logging to {0}\n'.format(filename))
        except Exception as e:
            self.perror('ERROR: {0!r}\n'.format(e))
        else:
            self.log_event('command', 'rec')
            self.recmode = True
            self.prompt = self.rec_prompt
            self.precmd = self._log_trace

    def stop_recording(self):
        """Stop the Hyperdeck recording."""
        if self.recmode:
            try:
                self.hd.send_command('stop')
                code, resp = self.hd.get_response()
                self.poutput(resp)
            except Exception as e:
                self.perror('ERROR: {0!r}\n'.format(e))
                self.hd = None
            self.logf.close()
            self.poutput('Logfile closed\n')
            self.prompt = self.std_prompt
            self.precmd = self._pass_cmd
            self.recmode = False

    def do_fail(self, args):
        """Generate an exception"""
        raise Exception("Abort session")

    def do_load(self, args):
        """load FILENAME: execute commands from a file."""
        if not args:
            self.perror('Missing filename\n')
        else:
            name = args.strip()
            try:
                with open(name, 'r') as f:
                    for line in f:
                        if not line.startswith('#'):
                            line = self.precmd(line.strip())
                            if line:
                                self.poutput('>'+line+'\n')
                                stop = self.onecmd(line)
                                if stop:
                                    return True
            except IOError as e:
                self.perror(str(e) + '\n')


@contextmanager
def lock_file(path):
    try:
        lockf = open(path, 'w')
        fcntl.flock(lockf, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError as e:
        raise SystemExit("Camclient instance still running")
    try:
        yield lockf
    finally:
        fcntl.flock(lockf, fcntl.LOCK_UN)
        lockf.close()
        os.unlink(path)


def send_message(relay, subject, body, sender, addrs):
    """
    Send a message to a list email addresses.

    @param subject: message subject.
    @param body: message contents.
    @param sender: message sender.
    @param addrs: list of email addresses.
    """
    msg = MIMEText(body, _subtype='plain')
    msg['From'] = sender
    msg['To'] = ", ".join(addrs)
    msg['Subject'] = subject
    s = smtplib.SMTP(relay)
    s.sendmail(sender, addrs, msg.as_string())
    s.close()


def main():
    abort_msg = """
    Warning, CAMHD client aborted with code:

      '{}'
    """
    # Create an OptionParser just to produce a usage message, HdcamApp
    # actually processes the command line.
    parser = OptionParser(usage=__doc__)
    parser.parse_args()

    relay = os.environ.get("SMTP_RELAY", "10.20.1.9")
    addrs = os.environ.get("CAMHD_ALERT", "").split(";")

    obj = HdcamApp()
    try:
        with lock_file("/var/lock/camhd.lock"):
            obj.cmdloop()
    except SystemExit as e:
        print(e.code, file=sys.stderr)
        if len(addrs) > 0:
            send_message(relay,
                         "CAMHD error",
                         abort_msg.format(e.code),
                         "camhd-noreply@uw.edu",
                         addrs)


if __name__ == '__main__':
    main()
