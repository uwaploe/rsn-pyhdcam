RSN HD Camera Software
=======================

Python package for the RSN HD-Camera project.


Library Prerequisites
---------------------

The development versions of the following libraries must be installed on the
system::

* libevent
* ZeroMQ
* libpython
* Python setuptools

The easiest way to install these libraries is with ``apt-get``::

    sudo apt-get install python-dev python-setuptools libevent-dev libzmq-dev


Python Prerequisites
--------------------

The file ``requirements.txt`` contains a list of required packages. These packages
should be installed under a virtual environment using ``pip``::

    sudo easy_install pip
    sudo pip install virtualenvwrapper
    mkvirtualenv hdcam
    pip install -r requirements.txt

Installation
-------------

Run the command below from within the ``hdcam`` virtual environment::

    python setup.py install


Subsystem Testing
-----------------

There are doc-test files for some of the subsystems which provide a quick test of the basic device operation (that it powers-on and communicates with the computer). More detailed testing instructions are available on the project wiki_.

.. _wiki: http://wavelet.apl.uw.edu/projects/rsn_hdcam/wiki

NCD Relay Board
~~~~~~~~~~~~~~~
::

    python -m doctest -v test/ncdcheck.txt


Pan/Tilt
~~~~~~~~
::

    python -m doctest -v test/ptcheck.txt


Attitude Sensor
~~~~~~~~~~~~~~~
::

    python -m doctest -v test/tcm6check.txt


Camera
~~~~~~
::

    python -m doctest -v test/camcheck.txt

Lights
~~~~~~
::
    python -m doctest -v test/light_check.txt

Network API
-----------

The `network api`_ is provided by the ``camserver.py`` program which runs as a service managed by
Runit_. The system will need to be configured to run services as an unprivileged user (in this case
the ``rsn`` user) by installing the following script as the ``runsvdir-rsn`` service::

    #!/bin/sh
    exec 2>&1
    exec chpst -e ./env -u rsn:dialout runsvdir /home/rsn/service

The ``env`` directory must contain settings for the ``HOME``, ``USER``, and ``LOGNAME`` environment
variables. You should read and understand the Runit documentation before attempting this.

Once this service is running, the contents of the ``sv`` subdirectory can be installed
under ``/home/rsn/service`` on the encode computer.

.. _network api: http://wavelet.apl.uw.edu/projects/rsn_hdcam/wiki/NetworkApi
.. _Runit: http://smarden.org/runit/