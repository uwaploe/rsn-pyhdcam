#!/usr/bin/env python
#
import unittest
from mock import Mock
from cStringIO import StringIO
import sys
import os
import gevent
import _socket
sys.path.insert(0, os.path.join('..', 'pyhdcam'))
from serialcom import Port
from ros import Rotator
from support import start_client, start_server

TEST_POSITION = 42


def rotator_command(node_id, cmd, data):
    r = None
    if cmd == '?':
        if data == 0:
            r = '%s,010,989,015,975,2,y,0007,2,1,03' % node_id
        else:
            r = '%se001' % node_id
    elif cmd == 'f':
        r = '%s%03d' % (node_id, TEST_POSITION)
    return r


def rotator_sim(ev, socket, addr):
    """
    Simulate the responses of an ROS Rotator.
    """
    def read_and_echo(n):
        buf = []
        for _ in range(n):
            c = socket.recv(1)
            buf.append(c)
            socket.send(c)
        return ''.join(buf)
    try:
        while True:
            msg = read_and_echo(2)
            if msg[1] != 'f':
                data = read_and_echo(3)
                resp = rotator_command(msg[0], msg[1], int(data, 10))
            else:
                resp = rotator_command(msg[0], msg[1], 0)
            if resp:
                socket.send(resp)
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()


class RotatorTest(unittest.TestCase):
    def setUp(self):
        self.port = Port('/dev/ttyUSB0')
        self.port.serial = Mock()
        self.pathname = '/tmp/port.sock'
        self.log = None
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, rotator_sim)

    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        self.port.close()
        if self.log:
            print self.log.getvalue()

    def test_basic(self):
        """Basic Rotator protocol test"""
        sock = start_client(self.pathname)
        self.port.serial.fileno.return_value = sock.fileno()
        self.port.open(sessionlog=self.log)
        rot = Rotator(self.port)
        self.assertEqual(rot.factory_ccw, 10)
        self.assertEqual(rot.factory_cw, 989)
        self.assertEqual(rot.ccw, 15)
        self.assertEqual(rot.cw, 975)
        p = rot.angle
        self.assertEqual(p, TEST_POSITION, 'Bad position: %r' % p)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(RotatorTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
