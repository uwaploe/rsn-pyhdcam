#!/usr/bin/env python
#
import struct
import random
import unittest
from mock import Mock
from cStringIO import StringIO
import sys
import os
import gevent
import _socket
sys.path.insert(0, os.path.join('..', 'pyhdcam'))
from serialcom import Port
from ncd import RelayBank, read_adc10
from support import start_client, start_server

def relay_sim(ev, socket, addr):
    """
    Simulate a set of NCD relay banks and 10-bit A/D
    """
    state = [0] * 33
    try:
        while True:
            start = ord(socket.recv(1))
            cmd = ord(socket.recv(1))
            if cmd == 140:
                new_state = ord(socket.recv(1))
            if cmd != 167 and cmd != 33:
                bank = ord(socket.recv(1))
            rval = chr(85)
            if 100 <= cmd < 108:
                bit = cmd - 100
                state[bank] &= ~(1 << bit)
            elif 108 <= cmd < 116:
                bit = cmd - 108
                state[bank] |= (1 << bit)
            elif 116 <= cmd < 124:
                bit = cmd - 116
                rval = chr((state[bank] & (1 << bit)) >> bit)
            elif cmd == 124:
                rval = chr(state[bank])
            elif cmd == 140:
                state[bank] = new_state
            elif cmd == 167:
                x = [random.randint(0, 1023) for _ in range(8)]
                rval = struct.pack('>8H', *x)
            n = len(rval)
            while n > 0:
                n -= socket.send(rval)
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()

class RelayBankTest(unittest.TestCase):
    def setUp(self):
        self.port = Port('/dev/ttyUSB0')
        self.port.serial = Mock()
        self.pathname = '/tmp/port.sock'
        self.log = None
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, relay_sim)
        self.sock = start_client(self.pathname)
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)

    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        self.port.close()
        if self.log:
            print self.log.getvalue()

    def test_0_basic(self):
        """Test RelayBank initialization"""
        rb = RelayBank(self.port)
        self.assertEqual(rb.bank, 1, 'Wrong default bank value: %r' % rb.bank)
        self.assertRaises(IndexError, rb.__getitem__, len(rb))
        self.assertTrue(rb.is_alive())

    def test_1_ops(self):
        """Test RelayBank list-mode operations"""
        rb = RelayBank(self.port)
        rb[2] = 1
        self.assertEqual(rb[2], 1, 'Relay set failed')
        rb[2] = 0
        self.assertEqual(rb[2], 0, 'Relay clear failed')
        rb[2] = True
        self.assertEqual(rb[2], 1, 'Relay set from boolean failed')
        x = 0xa5
        mask = 1
        for i in range(len(rb)):
            rb[i] = x & mask
            mask <<= 1
        self.assertEqual(int(rb), x, 'Unexpected relay setting: %d' % int(rb))

    def test_2_advanced(self):
        """Test advanced RelayBank list-mode operations"""
        rb = RelayBank(self.port)
        x = [1, 0, 1]
        rb[0:3] = x
        self.assertEqual(rb[0:3], x, 'Multi-element assignment failed')

    def test_3_callable(self):
        """Test RelayBank function-mode operations"""
        rb = RelayBank(self.port)
        x = 0x55
        rb(x)
        self.assertEqual(rb(), x, 'Relay assignment failed')

    def test_4_adc(self):
        """Test A/D reading"""
        r = read_adc10(self.port)
        self.assertEqual(len(r), 8, 'A/D read failed')
        for val in r:
            self.assertTrue(0 <= val < 1024, 'Out of range A/D value (%r)' % val)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(RelayBankTest)
    unittest.TextTestRunner(verbosity=2).run(suite)