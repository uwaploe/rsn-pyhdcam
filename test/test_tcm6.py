#!/usr/bin/env python
#
import unittest
from mock import Mock
from cStringIO import StringIO
import sys
import os
import gevent
from gevent.queue import Queue, Empty
import _socket
sys.path.insert(0, '..')
from pyhdcam.serialcom import Port
from pyhdcam.tcm6 import *
from support import start_client, start_server

def tcm6_sim(ev, socket, addr):
    """
    Simulate a TCM6
    """
    @coroutine
    def store_frame(outq):
        while True:
            frame = (yield)
            outq.put(frame)
    queue = Queue()
    parser = read_datagram(store_frame(queue))
    try:
        while True:
            c = socket.recv(1)
            if c:
                parser.send(ord(c))
            try:
                frame = queue.get_nowait()
                if frame.id == 'kGetModInfo':
                    resp = build_datagram(
                            build_frame('kModInfoResp', type='TCM6',
                                        revision='1234'))
                    socket.send(resp)
            except Empty:
                pass
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()

class Tcm6Test(unittest.TestCase):
    def setUp(self):
        self.port = Port('/dev/ttyUSB0')
        self.port.serial = Mock()
        self.pathname = '/tmp/port.sock'
        self.log = None
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, tcm6_sim)
        self.sock = start_client(self.pathname)

    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        self.port.close()
        if self.log:
            print self.log.getvalue()

    def test_protocol(self):
        """Test communication protocol."""
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        tcm = Tcm6(self.port)
        reply = tcm.kGetModInfo()
        self.assertEqual(reply.id, 'kModInfoResp', 'Unexpected reply (%r)' % reply)
        self.assertEqual(reply.payload.type, 'TCM6')
        self.assertEqual(reply.payload.revision, '1234')

    def test_timeout(self):
        """Test comm timeout detection"""
        bh = open('/dev/null', 'r')
        self.port.serial.fileno.return_value = bh.fileno()
        self.port.open(sessionlog=self.log)
        tcm = Tcm6(self.port, timeout=3)
        self.assertRaises(StopIteration, tcm.kGetModInfo)
        bh.close()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Tcm6Test)
    unittest.TextTestRunner(verbosity=2).run(suite)