#!/usr/bin/env python
#
# Unit tests for pyhdcam.camera module.
#
import unittest
from mock import Mock
from cStringIO import StringIO
import sys
import os
import gevent
import _socket
sys.path.insert(0, '..')
from pyhdcam.serialcom import Port
from pyhdcam.camera import Commands, Camera
from support import start_client, start_server

SETTINGS = """
%SubC 1Cam Alpha -1 v6.5.0 %Send $q for a list of commands
%Power=1 %VideoMode=1 %USB=0 %Internal_Laser=0 %LED=Auto %LED_Intensity=150 %LED_Delay=77 %LED_On-Time=60 %Intervals=0 %NightVision=0"""


def camera_sim(ev, socket, addr):
    valid = dict([(v[0], v) for k, v in Commands.__dict__.items()
                  if not k.startswith('_')])
    try:
        while True:
            cmd = socket.recv(2)
            if cmd[1] in valid:
                c, name = valid[cmd[1]]
                if name.endswith('='):
                    socket.send('{0}:{1}42\n'.format(c, name))
                else:
                    socket.send('{0}:{1}\n'.format(c, name))
            elif cmd[1] == '@':
                socket.send(SETTINGS)
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()


class CameraTest(unittest.TestCase):
    def setUp(self):
        self.port = Port('/dev/ttyUSB0')
        self.port.serial = Mock()
        self.pathname = '/tmp/port.sock'
        self.log = None
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, camera_sim)
        self.sock = start_client(self.pathname)

    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        self.port.close()
        if self.log:
            print self.log.getvalue()

    def test_0_basic(self):
        """Test basic commands"""
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        cam = Camera(self.port)
        status = cam.send(Commands.TOGGLE_LASER, timeout=2)
        self.assertTrue(status is not None, 'Command failed')
        self.assertEqual(cam['Internal_Laser'], '42')

    def test_1_invalid(self):
        """Test sending an invalid command"""
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        cam = Camera(self.port)
        status = cam.send(('?', None), timeout=2)
        self.assertTrue(status is None, 'Unexpected response')

    def test_2_log(self):
        """Test command logging"""
        codes = (Commands.MENU_DOWN, Commands.MENU_SELECT, Commands.MENU_LEFT,
                 Commands.MENU_SELECT)
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        cam = Camera(self.port)
        cam.start_logging()
        for c in codes:
            cam.send(c, timeout=2)
        seq = cam.stop_logging()
        check = tuple([e[1] for e in seq])
        self.assertEqual(check, codes, 'Unexpected result: %r' % repr(check))

    def test_3_settings(self):
        """Test parsing camera settings"""
        self.port.serial.fileno.return_value = self.sock.fileno()
        self.port.open(sessionlog=self.log)
        cam = Camera(self.port)
        names = cam.read_settings()
        self.assertTrue(names)
        self.assertEqual(cam['VideoMode'], '1')


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CameraTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
