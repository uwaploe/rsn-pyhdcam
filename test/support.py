#!/usr/bin/env python
#
# Support functions for testing
#
import gevent
from gevent import monkey
monkey.patch_socket()
import _socket
from gevent.event import Event
from gevent.server import StreamServer
from functools import partial

ready = Event()

def echo_server(ev, socket, addr):
    try:
        while True:
            data = socket.recv(256)
            gevent.sleep(0.5)
            socket.send(data)
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()

def start_server(pathname, handler):
    s = _socket.socket(_socket.AF_UNIX, _socket.SOCK_STREAM)
    s.setblocking(0)
    s.bind(pathname)
    s.listen(2)
    ev = Event()
    ready.set()
    svc = StreamServer(s, partial(handler, ev))
    svc.start()
    ev.wait()
    svc.stop()
    
def start_client(pathname):
    s = _socket.socket(_socket.AF_UNIX, _socket.SOCK_STREAM)
    s.setblocking(0)
    ready.wait()
    s.connect(pathname)
    ready.clear()
    return s

