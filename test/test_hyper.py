#!/usr/bin/env python
import unittest
import sys
import os
sys.path.insert(0, os.path.join('..', 'pyhdcam'))
from hyperdeck import Hyperdeck

CMDS = [
    ('play', {}, 'play'),
    ('play', {'speed': 50}, 'play: speed: 50'),
    ('play', {'speed': 80, 'loop': 'true'}, 'play: speed: 80 loop: true'),
    ('record', {'name': 'Clip 0001'}, 'record: name: "Clip 0001"'),
    ('configuration', {'video_input': 'SDI'}, 'configuration: video input: SDI')
]

class CommandTest(unittest.TestCase):
    def test_0_basic(self):
        """Test command formatting"""
        for cmd, params, expect in CMDS:
            line = Hyperdeck._build_command(cmd, **params)
            self.assertEqual(line, expect, 'Expected {0!r} got {1!r}'.format(expect, line))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CommandTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
