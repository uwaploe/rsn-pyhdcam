#!/usr/bin/env python
#
import unittest
from mock import Mock
import sys
import os
import gevent
sys.path.insert(0, os.path.join('..', 'pyhdcam'))
from serialcom import Port
from support import start_client, start_server, echo_server

class PortTest(unittest.TestCase):
    def setUp(self):
        self.port = Port('/dev/ttyUSB0')
        self.port.serial = Mock()
        self.pathname = '/tmp/port.sock'
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, echo_server)

    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        self.port.close()

    def test_port_io(self):
        """Test serial port i/o"""
        data_out = 'abcdefg'
        n_sub = 3
        sock = start_client(self.pathname)
        self.port.serial.fileno.return_value = sock.fileno()
        self.port.open()
        result = self.port.write(data_out, timeout=3)
        self.assertFalse(result, 'Unexpected result from Port.write: %r' % result)
        data_in = self.port.read(len(data_out), timeout=3)
        self.assertEqual(data_in, data_out, 'Read error: %r' % data_in)
        data_in = self.port.read(1024, timeout=2)
        self.assertFalse(data_in, 'Unexpected read result: %r' % data_in)
        result = self.port.write(data_out, timeout=3)
        self.assertFalse(result, 'Unexpected result from Port.write: %r' % result)
        data_in = self.port.read(n_sub, timeout=3)
        self.assertEqual(data_in, data_out[:n_sub], 'Read error: %r' % data_in)

    def test_errors(self):
        """Test error handling"""
        sock = start_client(self.pathname)
        self.port.serial.fileno.return_value = sock.fileno()
        self.assertRaises(IOError, self.port.write, 'abc', timeout=3)
        self.assertRaises(IOError, self.port.read, 1, timeout=3)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(PortTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
