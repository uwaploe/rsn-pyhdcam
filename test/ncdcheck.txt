NCD Relay/ADC Checkout
======================

Initial imports::

    >>> from pyhdcam.serialcom import Port
	>>> from pyhdcam.ncd import RelayBank, read_adc10

Open serial port::

	>>> usb = Port('/dev/ttyUSB0', baud=115200)
	>>> usb.open()

Initialize RelayBank object and check comms::

	>>> rb = RelayBank(usb)
	>>> rb.is_alive()
	True

Verify A/D access::

	>>> rec = read_adc10(usb)
	>>> len(rec) == 8
	True
