#!/usr/bin/env python
#
import unittest
from mock import Mock
from cStringIO import StringIO
import sys
import os
import gevent
import _socket
sys.path.insert(0, os.path.join('..', 'pyhdcam'))
from serialcom import Port
from ros import Light
from support import start_client, start_server

TEST_THERM = 470
TEST_TEMP = '21.3'


def light_command(node_id, cmd, data):
    r = None
    if cmd == '?':
        if data == 5:
            r = '{0}p{1:03d}'.format(node_id, light_command.intensity)
        elif data == 0:
            r = '%s,000,000,000,000,2,y,0017,1,4,06' % node_id
        else:
            r = '%se001' % node_id
    elif cmd == 'f':
        r = '%s%03d' % (node_id, TEST_THERM)
    elif cmd == 'l':
        light_command.intensity = data
    return r
light_command.intensity = 0


def light_sim(ev, socket, addr):
    """
    Simulate the responses of an ROS Light.
    """
    def read_and_echo(n):
        buf = []
        for _ in range(n):
            c = socket.recv(1)
            buf.append(c)
            socket.send(c)
        return ''.join(buf)
    try:
        while True:
            msg = read_and_echo(2)
            if msg[1] != 'f':
                data = read_and_echo(3)
                resp = light_command(msg[0], msg[1], int(data, 10))
            else:
                resp = light_command(msg[0], msg[1], 0)
            if resp:
                socket.send(resp)
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()


class LightTest(unittest.TestCase):
    def setUp(self):
        self.port = Port('/dev/ttyUSB0')
        self.port.serial = Mock()
        self.pathname = '/tmp/port.sock'
        self.log = None
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, light_sim)

    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        self.port.close()
        if self.log:
            print self.log.getvalue()

    def test_basic(self):
        """Basic Light protocol test"""
        sock = start_client(self.pathname)
        self.port.serial.fileno.return_value = sock.fileno()
        self.port.open(sessionlog=self.log)
        l = Light(self.port)
        l.intensity = 50
        self.assertEqual(light_command.intensity, 50, 'Error setting intensity')
        self.assertEqual(str(l.temperature), TEST_TEMP,
                         'Bad temperature value: %s' % str(l.temperature))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(LightTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
