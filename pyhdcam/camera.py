#!/usr/bin/env python
#
"""
.. :module: camera
     :synopsis: Interface to the SubC 1Cam Alpha HD Video Camera
"""
import time
import gevent
import simplejson as json
import curses
import curses.ascii as ascii


class Commands(object):
    ZOOM_IN = ('D3', '')
    ZOOM_OUT = ('D4', '')
    ZOOM_SLOW = ('Z*1', '')
    MENU_UP = ('D5', 'up')
    MENU_LEFT = ('D6', 'left')
    MENU_SELECT = ('D7', 'center')
    MENU_RIGHT = ('D8', 'right')
    MENU_DOWN = ('D9', 'down')
    MENU = ('PM', 'menu')
    FOCUS_AUTO = ('FA', '')
    FOCUS_NEAR = ('FN', '')
    FOCUS_FAR = ('FF', '')
    TOGGLE_POWER = ('P0', 'power')
    TOGGLE_VIDEO = ('Pv', 'videomode')
    TOGGLE_USB = ('Pu', 'usb')
    TOGGLE_IR = ('Pn', 'nightvision')
    TOGGLE_LASER = ('Pl', 'internal_laser')
    RESET_EEPROM = ('Z^', '')
    GET_TEMPERATURE = ('Zt', 'temperature')


def parse_settings(lines):
    """
    Parse the raw output from the $Z@ command into a
    dictionary of camera settings.
    """
    d = {}
    # Skip the first two lines
    for line in lines[2:]:
        if ':' in line:
            name, value = line.strip().split(':', 1)
            d[name.lower()] = value
    return d


class Camera(object):
    """
    Class to interface with a SubC 1Cam Alpha via its rs232 control
    interface.
    """
    def __init__(self, port):
        """
        :param port: serial port interface
        :type port: pyhdcam.serialcom.Port
        """
        assert port.isopen()
        self.port = port
        self.log = None
        self.tlast = None
        self._settings = {}

    def __getitem__(self, name):
        return self._settings.get(name.lower())

    def __setitem__(self, name, val):
        self._settings[name] = val

    # Support dictionary methods through delegation
    def __getattr__(self, name):
        return getattr(self._settings, name)

    def read_settings(self):
        """
        Read the current camera settings. Returns a list of
        parameter names.
        """
        self.port.write(b'$Z@', timeout=4)
        lines = self.timed_read(3)
        self._settings = parse_settings(lines)
        return self._settings.keys()

    def readline(self, timeout=None):
        """
        Read the output from the camera up to the next linefeed.

        :param timeout: read timeout in seconds or ``None`` to block.
        :rtype: string or ``None``
        """
        text = []
        done = False
        while not done:
            c = self.port.read(1, timeout=timeout)
            if not c:
                return None
            if c == '\n':
                done = True
            else:
                text.append(c)
        return ''.join(text)

    def timed_read(self, timeout):
        """
        Read the output from the camera until *timeout* seconds elapses
        between characters.

        :rtype: list of lines.
        """
        resp = self.port.read(1024, timeout=timeout)
        if resp:
            return resp.split('\n')
        else:
            return []

    def flush_input(self):
        """
        Flush all content from the input buffer.
        """
        data = self.port.read(128, timeout=0)
        while data:
            data = self.port.read(128, timeout=0)

    def send(self, command, timeout=None):
        """
        Send a command to the camera.

        :param command: Commands class attribute.
        :return: response string or ``None``
        """
        code, feedback = command
        cmd = b'$' + code
        if self.log is not None:
            if not self.tlast:
                self.tlast = time.time()
            t0 = time.time()
            t = int((t0 - self.tlast) * 1000)
            self.log.append((t, command, timeout))
            self.tlast = t0
        self.port.write(cmd, timeout=timeout)
        resp = self.readline(2)
        self.flush_input()
        return resp

    def start_logging(self):
        """
        Start recording all commands for later replay. This is useful for
        recording a command sequence which manipulates the camera menu.
        """
        self.log = list()
        self.tlast = 0

    def stop_logging(self):
        """
        Stop recording commands.

        :return: recorded command sequence
        """
        cmds = self.log
        self.log = None
        return cmds

    def replay(self, commands):
        """
        Re-send a list of commands.

        :param commands: command sequence returned by stop_logging
        """
        for delay, command, timeout in commands:
            gevent.sleep(delay / 1000)
            code = (str(command[0]), str(command[1]))
            self.send(code, timeout=timeout)


def interact(cam, logfile=None):
    """
    Manage an interactive session with a Camera. Key-presses are interpreted as
    Camera commands and optionally recorded to *logfile*.

    :param cam: Camera object
    :type cam: Camera
    :param logfile: file to log commands
    """
    cmd_dict = {
        curses.KEY_UP: Commands.MENU_UP,
        curses.KEY_DOWN: Commands.MENU_DOWN,
        curses.KEY_LEFT: Commands.MENU_LEFT,
        curses.KEY_RIGHT: Commands.MENU_RIGHT,
        curses.KEY_ENTER: Commands.MENU_SELECT,
        ord('\n'): Commands.MENU_SELECT,
        ord('\r'): Commands.MENU_SELECT,
        ord('s'): Commands.MENU_SELECT,
        ord('+'): Commands.ZOOM_IN,
        ord('-'): Commands.ZOOM_OUT
    }

    def session(stdscr):
        stdscr.addstr(0, 0, 'Enter 1Cam command codes or <ESC> to exit...')
        stdscr.addstr(1, 0, 'Response: ', curses.A_BOLD)
        stdscr.refresh()
        quit_cmds = (ord('Q'), ord('q'), ascii.ESC, ascii.ctrl('d'))
        while True:
            c = stdscr.getch()
            if c in quit_cmds:
                break
            cmd = cmd_dict.get(c)
            if cmd:
                resp = cam.send(cmd, timeout=5)
                if resp is not None:
                    stdscr.move(1, 12)
                    stdscr.clrtoeol()
                    stdscr.addstr(1, 12, resp)
                    stdscr.refresh()
    codes = [getattr(Commands, c) for c in dir(Commands)
             if ascii.isupper(c[0])]
    for code, feedback in codes:
        cmd_dict[ord(code[-1])] = (code, feedback)
    if logfile:
        cam.start_logging()
    curses.wrapper(session)
    if logfile:
        log = cam.stop_logging()
        json.dump(log, logfile)


if __name__ == '__main__':
    from pyhdcam.serialcom import Port
    import sys
    try:
        devname = sys.argv[1]
    except IndexError:
        sys.stderr.write('Usage: camera.py serialdev [logfile]\n')
        sys.exit(1)
    p = Port(devname, baud=9600)
    p.open()
    cam = Camera(p)
    cam.flush_input()
    if len(sys.argv) > 2:
        with open(sys.argv[2], 'w') as logfile:
            interact(cam, logfile)
    else:
        interact(cam)
