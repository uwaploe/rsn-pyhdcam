#!/usr/bin/env python
"""
.. :module: hyperdeck
     :synopsis: Control a BlackMagic Hyperdeck video recorder.
"""
import socket

HYPERDECK_PORT = 9993
CRLF = '\r\n'


class ProtocolError(Exception):
    pass


class CommandError(EnvironmentError):
    pass


class Hyperdeck(object):
    def __init__(self, host, port=HYPERDECK_PORT):
        self.host = host
        self.port = port
        self.sock = socket.create_connection((host, port))
        self.file = self.sock.makefile('rb')
        code, resp = self.get_response()
        if code != 500:
            raise ProtocolError('No response')

    def _sendline(self, line):
        self.sock.sendall('{0}{1}'.format(line, CRLF))

    def __del__(self):
        self._sendline('quit')

    @staticmethod
    def _build_command(cmd, **params):
        if len(params) == 0:
            return cmd
        else:
            parts = [cmd + ':']
            for k, v in params.items():
                parts.append(k.replace('_', ' ') + ':')
                v = str(v)
                if ' ' in v:
                    v = '"{0}"'.format(v)
                parts.append(v)
            return ' '.join(parts)

    def send_command(self, cmd, **params):
        self._sendline(self._build_command(cmd, **params))

    def get_response(self):
        lines = []
        code = 0
        msg = ''
        while True:
            line = self.file.readline()
            if not line:
                raise ProtocolError('EOF')
            # Response is terminated by a blank line
            if line == CRLF:
                break
            lines.append(line)

            # Error responses and "ok" responses are *not* terminated
            # by a blank line (yay for consistency!) so we need to
            # check the result code in the first line.
            if len(lines) == 1:
                code, msg = line.strip().split(' ', 1)
                code = int(code)
                if code <= 200:
                    break
        # Turn Hyperdeck errors into exceptions
        if code < 200:
            raise CommandError((code, msg))
        return code, ''.join(lines)


if __name__ == '__main__':
    hd = Hyperdeck('10.0.97.170')
    hd.send_command('clips get')
    code, resp = hd.get_response()
    print code
    print resp
