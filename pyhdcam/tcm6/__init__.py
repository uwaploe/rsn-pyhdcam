#!/usr/bin/env python
#
from frames import *
import gevent
from gevent.queue import Queue, Empty


def reader(port, queue):
    """
    Greenlet task to read and parse data frames from the TCM6. Bytes are
    read from a serial port and parsed frames are output to a queue.
    """
    @coroutine
    def get_frame():
        while True:
            frame = (yield)
            queue.put(frame)

    parser = read_datagram(get_frame())
    while True:
        c = port.read(1)
        if c:
            parser.send(ord(c))


class FrameTimeout(Exception):
    pass


class Tcm6(object):
    """
    Class to represent a TCM6, tilt-compensated compass module attached to a
    serial port. TCM6 commands are implemented as object methods.

    :var port: serial port interface. Needs to provide read and write methods.
    :var reader: task to read device output
    :var timeout: command response timeout in seconds
    :var mode: device operating mode, 'normal' or 'sync'
    """
    def __init__(self, port, timeout=None):
        """
        Initialize a class instance.

        :param port: serial port interface
        :type port: serialcom.Port
        :param timeout: command response timeout in seconds
        """
        assert port.isopen()
        self.port = port
        self.queue = Queue()
        self.reader = gevent.spawn(reader, self.port, self.queue)
        self._timeout = timeout
        self.mode = 'normal'

    def __del__(self):
        self.reader.kill(timeout=3)

    def __iter__(self):
        return self

    @property
    def timeout(self):
        """
        Command timeout in seconds or ``None``
        """
        return self._timeout

    @timeout.setter
    def timeout(self, val):
        self._timeout = val

    def wakeup(self):
        """
        Send wakeup character to the device. This is only needed if the device
        is in SYNC mode. See section 7.3.34 of the manual.
        """
        self.port.write('\xff', timeout=self._timeout)
        gevent.sleep(7e-4)

    def next(self):
        """
        When used as a iterator, return the next data frame. This is only
        useful if the device is in 'push' (interval) sampling mode.
        """
        try:
            frame = self.queue.get(timeout=self._timeout)
            if frame.id == 'kSetModeResp':
                self.mode = frame.payload.mode_id
            elif frame.id == 'kPowerDownDone':
                self.mode = 'sleep'
            else:
                self.mode = 'normal'
            return reformat_payload(frame)
        except Empty:
            raise StopIteration

    def __getattr__(self, name):
        """
        Implement the TCM6 commands as methods. Command parameters are passed
        as keyword arguments. Command names match those listed in Table 7-1 of
        the manual. See command definitions in frames.py for the parameter
        names.

        Examples:
            obj.kSetConfig(id='kMountingRef', value='xup_180')
            obj.kGetModInfo()
        """
        def _send_command(**params):
            packet = build_datagram(build_frame(name, **params))
            self.port.write(packet, timeout=self._timeout)
            # Any character will wake the device from sleep
            if self.mode == 'sleep':
                self.mode = 'normal'
            if name in no_response:
                return None
            else:
                return self.next()
        if self.mode == 'sync':
            self.wakeup()
        return _send_command


def test(devname):
    from pyhdcam import serialcom
    import sys
    port = serialcom.Port(devname, baud=38400)
    port.open(sessionlog=sys.stdout)
    tcm6 = Tcm6(port, timeout=3)
    print tcm6.kGetModInfo()
    print tcm6.kPowerDown()
    gevent.sleep(2)
    tcm6.wakeup()
    print tcm6.next()
    print tcm6.kGetData()
    port.close()

if __name__ == '__main__':
    test('/dev/ttyMI4')
