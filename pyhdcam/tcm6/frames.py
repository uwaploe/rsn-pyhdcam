#!/usr/bin/env python
#
# TCM6 communication protocol data structures
#
from construct import *
from array import array

component_id = Enum(Byte('id'),
                    kHeading = 5,
                    kTemperature = 7,
                    kDistortion = 8,
                    kCalStatus = 9,
                    kPAligned = 21,
                    kRAligned = 22,
                    kIZAligned = 23,
                    kPAngle = 24,
                    kRAngle = 25,
                    KXAligned = 27,
                    KYAligned = 28,
                    KZAligned = 29)

config_id = Enum(Byte('id'),
                 kDeclination = 1,
                 kTrueNorth = 2,
                 kBigEndian = 6,
                 kMountingRef = 10,
                 kUserCalNumPoints = 12,
                 kUserCalAutoSampling = 13,
                 kBaudRate = 14)

mounting_ref = Enum(Byte('mount'),
                    std_0 = 1,
                    xup_0 = 2,
                    yup_0 = 3,
                    std_90 = 4,
                    std_180 = 5,
                    std_270 = 6,
                    zdown_0 = 7,
                    xup_90 = 8,
                    xup_180 = 9,
                    xup_270 = 10,
                    yup_90 = 11,
                    yup_180 = 12,
                    yup_270 = 13,
                    zdown_90 = 14,
                    zdown_180 = 15,
                    zdown_270 = 16)

baud_rate = Enum(Byte('speed'),
                 b300 = 0,
                 b600 = 1,
                 b1200 = 2,
                 b1800 = 3,
                 b2400 = 4,
                 b3600 = 5,
                 b4800 = 6,
                 b7200 = 7,
                 b9600 = 8,
                 b14400 = 9,
                 b19200 = 10,
                 b28800 = 11,
                 b38400 = 12,
                 b57600 = 13,
                 b115200 = 14)

cal_type = Enum(UBInt32('type'),
                cal_full_range = 10,
                cal_2d = 20,
                cal_hi = 30,
                cal_limit_tilt = 40,
                cal_accel_only = 100,
                cal_accel_mag = 110)

data_value = Struct('pair',
                    component_id,
                    Switch('value',
                           lambda ctx: ctx['id'],
                           {'kHeading': BFloat32('heading'),
                            'kTemperature': BFloat32('temp'),
                            'kDistortion': UBInt8('isdistorted'),
                            'kCalStatus': UBInt8('iscal'),
                            'kPAligned': BFloat32('pitch_cal'),
                            'kRAligned': BFloat32('roll_cal'),
                            'kIZAligned': BFloat32('iz_cal'),
                            'kPAngle': BFloat32('pitch'),
                            'kRAngle': BFloat32('roll'),
                            'KXAligned': BFloat32('xmag_cal'),
                            'KYAligned': BFloat32('ymag_cal'),
                            'KZAligned': BFloat32('izmag_cal')}))

config_value = Struct('pair',
                      config_id,
                      Switch('value',
                             lambda ctx: ctx['id'],
                             {'kDeclination': BFloat32('decl'),
                              'kTrueNorth': UBInt8('istrue'),
                              'kBigEndian': UBInt8('isbigendian'),
                              'kMountingRef': mounting_ref,
                              'kUserCalNumPoints': UBInt32('points'),
                              'kUserCalAutoSampling': UBInt8('autosample'),
                              'kBaudRate': baud_rate}))

axis_id = Enum(UBInt8('axis_id'),
               kXAxis = 1,
               kYAxis = 2,
               kZAxis = 3)

mode_id = Enum(UBInt8('mode_id'),
               normal = 0,
               sync = 100)

filter_params = Struct('filterParams',
                       UBInt8('param_id'),
                       axis_id,
                       UBInt8('count'),
                       Array(lambda ctx: ctx['count'], BFloat64('value')))

acq_params = Struct('acqParams',
                    UBInt8('polling'),
                    UBInt8('flush'),
                    BFloat32('acq_interval'),
                    BFloat32('resp_interval'))

#: kModInfoResp (frame ID 2)
mod_info_resp = Struct('modInfoResp',
                       String('type', 4),
                       String('revision', 4))

#: kSetDataComponents (frame ID 3)
set_data_components = Struct('setDataComponents',
                             UBInt8('count'),
                             Array(lambda ctx: ctx['count'], component_id))


#: kDataResp (frame ID 5)
data_response = Struct('dataResponse',
                       UBInt8('count'),
                       Array(lambda ctx: ctx['count'], data_value))

#: kSetConfig (frame ID 6)
set_config = Rename('setConfig', config_value)

#: kGetConfig (frame ID 7)
get_config = Struct('getConfig', config_id)

#: kConfigResp (frame ID 8)
config_resp = Rename('configResp', config_value)

#: kStartCal (frame ID 10)
start_cal = Struct('startCal', cal_type)

#: kSetParam (frame ID 12)
set_param = Rename('setParam', filter_params)

#: kGetParam (frame ID 13)
get_param = Struct('getParam', UBInt8('param_id'), axis_id)

#: kParamResp (frame ID 14)
param_resp = Rename('paramResp', filter_params)

#: kSaveDone (frame ID 16)
save_done = Struct('saveDone', UBInt16('error'))

#: kUserCalSampCount (frame ID 17)
cal_samp_count = Struct('userCalSampCount', UBInt32('count'))

#: kUserCalScore (frame ID 18)
cal_score = Struct('userCalScore',
                   BFloat32('mag_score'),
                   BFloat32('param2'),
                   BFloat32('accel_score'),
                   BFloat32('dist_err'),
                   BFloat32('tilt_err'),
                   BFloat32('tilt_range'))

#: kSetAcqParams (frame ID 24)
set_acq_params = Rename('setAcqParams', acq_params)

#: kAcqParamsResp (frame ID 27)
acq_params_resp = Rename('acqParamsResp', acq_params)

#: kSetMode (frame ID 46)
set_mode = Struct('setMode', mode_id)

#: kSetModeResp (frame ID 47)
set_mode_resp = Struct('setModeResp', mode_id)

#: Data frame
data_frame = Struct('frame',
                    Enum(Byte('id'),
                         kGetModInfo = 1,
                         kModInfoResp = 2,
                         kSetDataComponents = 3,
                         kGetData = 4,
                         kDataResp = 5,
                         kSetConfig = 6,
                         kGetConfig = 7,
                         kConfigResp = 8,
                         kSave = 9,
                         kStartCal = 10,
                         kStopCal = 11,
                         kSetParam = 12,
                         kGetParam = 13,
                         kParamResp = 14,
                         kPowerDown = 15,
                         kSaveDone = 16,
                         kUserCalSampCount = 17,
                         kUserCalScore = 18,
                         kSetConfigDone = 19,
                         kSetParamDone = 20,
                         kStartIntervalMode = 21,
                         kStopIntervalMode = 22,
                         kPowerUp = 23,
                         kSetAcqParams = 24,
                         kGetAcqParams = 25,
                         kAcqParamsDone = 26,
                         kAcqParamsResp = 27,
                         kPowerDownDone = 28,
                         kFactoryUserCal = 29,
                         kFactoryUserCalDone = 30,
                         kTakeUserCalSample = 31,
                         kFactoryInclCal = 36,
                         kFactoryInclCalDone = 37,
                         kSetMode = 46,
                         kSetModeResp = 47,
                         kSyncRead = 49),
                    Switch('payload',
                           lambda ctx: ctx['id'],
                           {'kGetModInfo': Pass,
                            'kModInfoResp': mod_info_resp,
                            'kSetDataComponents': set_data_components,
                            'kGetData': Pass,
                            'kDataResp': data_response,
                            'kSetConfig': set_config,
                            'kGetConfig': get_config,
                            'kConfigResp': config_resp,
                            'kSave': Pass,
                            'kStartCal': start_cal,
                            'kStopCal': Pass,
                            'kSetParam': set_param,
                            'kGetParam': get_param,
                            'kParamResp': param_resp,
                            'kPowerDown': Pass,
                            'kSaveDone': save_done,
                            'kUserCalSampCount': cal_samp_count,
                            'kUserCalScore': cal_score,
                            'kSetConfigDone': Pass,
                            'kSetParamDone': Pass,
                            'kStartIntervalMode': Pass,
                            'kStopIntervalMode': Pass,
                            'kPowerUp': Pass,
                            'kSetAcqParams': set_acq_params,
                            'kGetAcqParams': Pass,
                            'kAcqParamsDone': Pass,
                            'kAcqParamsResp': acq_params_resp,
                            'kPowerDownDone': Pass,
                            'kFactoryUserCal': Pass,
                            'kFactoryUserCalDone': Pass,
                            'kTakeUserCalSample': Pass,
                            'kFactoryInclCal': Pass,
                            'kFactoryInclCalDone': Pass,
                            'kSetMode': set_mode,
                            'kSetModeResp': set_mode_resp,
                            'kSyncRead': Pass}))

#: Commands from which there is no response
no_response = ('kStartCal', 'kStopCal', 'kSetDataComponents',
               'kStartIntervalMode', 'kStopIntervalMode')

datagram = Struct('datagram',
                  UBInt16('count'),
                  Field('contents', lambda ctx: ctx['count'] - 4),
                  UBInt16('crc'))

def reformat_payload(frame):
    """
    Reformat the payload attribute of the following frame types:

    kDataResp
    kConfigResp

    >>> f = Container(id = 'kDataResp', payload = Container(count = 3, pair = [Container(id = 'kHeading', value = 333.67697143554688), Container(id = 'kPAngle', value = -15.116789817810059), Container(id = 'kRAngle', value = -0.054749440401792526)]))
    >>> reformat_payload(f)
    Container({'payload': Container({'kRAngle': -0.054749440401792526, 'kPAngle': -15.116789817810059, 'kHeading': 333.67697143554688}), 'id': 'kDataResp'})
    >>> f = Container(id = 'kConfigResp', payload = Container(id = 'kMountingRef', value = 'std_0'))
    >>> reformat_payload(f)
    Container({'payload': Container({'kMountingRef': 'std_0'}), 'id': 'kConfigResp'})
    """
    if frame.id == 'kDataResp':
        values = {}
        for container in frame.payload.pair:
            values[container.id] = container.value
        frame.payload = Container(**values)
    elif frame.id == 'kConfigResp':
        values = {frame.payload.id: frame.payload.value}
        frame.payload = Container(**values)
    return frame
        
def build_frame(frame_type, **kwds):
    r"""
    Construct a new data frame.

    :param frame_type: frame type name
    :type frame_type: string
    :param kwds: parameter name/value pairs
    :return: frame contents as a string

    >>> build_frame('kGetModInfo')
    '\x01'
    >>> build_frame('kSetConfig', id='kTrueNorth', value=1)
    '\x06\x02\x01'
    >>> build_frame('kSetConfig', id='kMountingRef', value='xup_180')
    '\x06\n\t'
    >>> build_frame('kSetMode', mode_id='normal')
    '.\x00'
    >>> build_frame('kSetMode', mode_id='sync')
    '.d'
    >>> build_frame('kSetAcqParams', polling=1, flush=0, acq_interval=0, resp_interval=0)
    '\x18\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    """
    if kwds:
        payload = Container(**kwds)
    else:
        payload = None
    return data_frame.build(Container(id=frame_type,
                                      payload=payload))

def build_datagram(contents):
    r"""
    Construct a datagram (packet)

    :param contents: data frame contents
    :type contents: string
    :return: datagram contents as a string.

    >>> build_datagram(build_frame('kGetModInfo'))
    '\x00\x05\x01\xef\xd4'
    """
    def calc_crc(bytes):
        c = 0
        for byte in bytes:
            c = update_crc(byte, c)
        return c
    count = len(contents) + 4
    # CRC calculation includes the Datagram Size field
    buf = array('B', [(count << 8) & 0xff, count & 0xff])
    buf.fromstring(contents)
    crc = calc_crc(buf)
    return datagram.build(Container(count=count,
                                    contents=contents,
                                    crc=crc))
    
class BadCrc(Exception):
    """
    Exception raised on CRC check failure.

    :var sent: CRC included in the packet.
    :var calc: CRC calculated from packet contents.
    """
    def __init__(self, sent, calc):
        self.sent = sent
        self.calc = calc

    def __str__(self):
        return 'sent={0:04x} calc={1:04x}'.format(self.sent, self.calc)

def update_crc(newval, crc=0):
    crc = ((crc << 8) | ((crc >> 8) & 0xff))
    crc = crc ^ newval
    crc = crc ^ ((crc & 0xff) >> 4)
    crc = crc ^ ((crc << 8) << 4)
    crc = crc ^ (((crc & 0xff) << 4) << 1)
    return crc & 0xffff

def coroutine(func):
    def start(*args,**kwargs):
        cr = func(*args,**kwargs)
        cr.next()
        return cr
    return start

@coroutine
def read_datagram(target):
    """
    Coroutine to continuously parse datagrams. The parsed datagrams are
    sent to the supplied target.
    """
    while True:
        crc = 0
        msb = (yield)
        crc = update_crc(msb, crc)
        lsb = (yield)
        crc = update_crc(lsb, crc)
        remaining = lsb + msb*256 - 2
        if remaining <= 0:
            continue
        contents = array('B')
        while remaining > 2:
            byte = (yield)
            crc = update_crc(byte, crc)
            contents.append(byte)
            remaining -= 1
        msb = (yield)
        lsb = (yield)
        check = lsb + msb*256
        if check != crc:
            raise BadCrc(check, crc)
        frame = data_frame.parse(contents.tostring())
        target.send(frame)
        # Throw away the first byte received after power-up
        if frame.id == 'kPowerDownDone':
            _ = (yield)
        
@coroutine
def frame_display():
    while True:
        frame = (yield)
        print frame

__all__ = ['reformat_payload', 'build_frame', 'build_datagram', 'BadCrc',
           'read_datagram', 'frame_display', 'coroutine', 'no_response']
                         
if __name__ == "__main__":
    import doctest
    doctest.testmod()
