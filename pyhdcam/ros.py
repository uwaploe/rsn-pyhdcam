#!/usr/bin/env python
#
"""
.. module:: ros
   :synopsis: Interface to ROS single-axis rotators.
"""
import time
import math
from decimal import Decimal
from utils import Calibration


class CommTimeout(Exception):
    """Raised when no response is received from the device."""
    pass


class Node(object):
    """
    Represent a node on an ROS serial network
    """
    def __init__(self, port, node_id='A'):
        """
        Initialize a new class instance.

        .. note::

           port must be open.

        :param port: interface to serial port.
        :type port: serialcom.Port
        :param node_id: rs485 network ID for this device
        """
        assert port.isopen()
        self.port = port
        self.node_id = node_id
        resp = self.send('?000', feedback=33)
        digits = '0123456789'
        self._settings = []
        for f in resp.split(','):
            if f[0] in digits:
                self._settings.append(int(f, 10))
            else:
                self._settings.append(f)

    def __repr__(self):
        return '{0}({1!r}, node_id={2!r})'.format(
            self.__class__.__name__,
            self.port, self.node_id)

    def send(self, command, feedback=0, timeout=3):
        """
        Send a command to the device and optionally read the feedback.

        :param command: command string
        :param feedback: number of characters expected
        :type feedback: integer or None
        :param timeout: response timeout in seconds
        :return: feedback from the device or None
        """
        text = self.node_id + command
        for c in text:
            self.port.write(c, timeout=timeout)
            resp = self.port.read(1, timeout=timeout)
            if resp != c:
                raise CommTimeout
        if feedback:
            return self.port.read(feedback, timeout=timeout)
        else:
            return None


class Rotator(Node):
    """
    Class to provide an interface to an ROS rotator.
    """
    @classmethod
    def degrees_per_sec(cls, val):
        return val * 0.5

    @classmethod
    def counts_per_sec(cls, val):
        return int(val * 2)

    @property
    def factory_ccw(self):
        """
        The factory counter-clockwise limit in degrees
        """
        return self.degrees(self._settings[1])

    @property
    def factory_cw(self):
        """
        The factory clockwise limit in degrees
        """
        return self.degrees(self._settings[2])

    def degrees(self, counts):
        """
        Convert rotator counts to degrees of shaft rotation.
        """
        ccw = self._settings[1]
        cw = self._settings[2]
        return round(360 * float(counts - ccw) / float(cw - ccw), 1)

    def counts(self, degrees):
        """
        Convert degrees to rotator counts.
        """
        ccw = self._settings[1]
        cw = self._settings[2]
        scale = degrees / 360.
        return int(scale * (cw - ccw) + ccw + 0.5)

    def start_cw(self, speed, ramp=True):
        """
        Start a clockwise rotation.

        :param speed: desired speed setting in deg/sec
        :param ramp: if ``True``, ramp speed up using the current
                     acceleration setting.
        :return: actual speed
        """
        cmd = ramp and '+' or '>'
        actual = Rotator.counts_per_sec(speed)
        self.send('{0}{1:03d}'.format(cmd, actual))
        return Rotator.degrees_per_sec(actual)

    def start_ccw(self, speed, ramp=True):
        """
        Start a counter-clockwise rotation.

        :param speed: desired speed setting in deg/sec
        :param ramp: if ``True``, ramp speed up using the current
                     acceleration setting.
        :return: actual speed
        """
        cmd = ramp and '-' or '<'
        actual = Rotator.counts_per_sec(speed)
        self.send('{0}{1:03d}'.format(cmd, actual))
        return Rotator.degrees_per_sec(actual)

    def stop(self, brake, ramp=True):
        """
        Stop the current rotation by applying a braking force.

        :param brake: brake force setting in counts
        :param ramp: if ``True``, ramp speed down using the current
                     acceleration setting.
        """
        cmd = ramp and 't' or 's'
        self.send('{0}{1:03d}'.format(cmd, brake))

    def apply_brake(self, brake):
        """
        Apply a braking torque to the motor by starting a rotation and
        then immediately stopping. It is not enough to simply send the
        stop command as the motor will not apply the braking torque unless
        a rotation has been initiated.
        """
        self.start_cw(0.5, ramp=False)
        time.sleep(0.1)
        self.stop(brake, ramp=False)

    @property
    def angle(self):
        """
        The current rotator position in degrees.
        """
        resp = self.send('f', feedback=4)
        return self.degrees(int(resp[1:], 10))

    @property
    def ccw(self):
        """
        The user counter-clockwise limit in degrees
        """
        return self.degrees(self._settings[3])

    @ccw.setter
    def ccw(self, value):
        if not (self.factory_ccw <= value <= self.cw <= self.factory_cw):
            raise ValueError('Invalid user-ccw limit')
        value = self.counts(value)
        self.send('d{0:03d}'.format(value))
        self._settings[3] = value

    @property
    def cw(self):
        """
        The user clockwise limit in degrees
        """
        return self.degrees(self._settings[4])

    @cw.setter
    def cw(self, value):
        if not (self.factory_ccw <= self.ccw <= value <= self.factory_cw):
            raise ValueError('Bad user-cw limit')
        value = self.counts(value)
        self.send('u{0:03d}'.format(value))
        self._settings[4] = value

    def moveto(self, position):
        """
        Use the new positioning command to move the rotator to an
        absolute position. This operation uses the current ramp
        parameters (acceleration and maximum velocity).

        :param position: desired position in degrees.
        """
        if not (self.ccw <= position <= self.cw):
            raise ValueError('New position out of range')
        position = self.counts(position)
        self.send('p{0:03d}'.format(position))

    def move(self, offset):
        """
        Adjust the position by *offset* counts relative to the current
        position.

        :param offset: relative position in degrees.
        """
        self.moveto(self.angle + offset)

    @property
    def accel(self):
        """
        The rotator acceleration value in deg/s^2
        """
        resp = self.send('?003', feedback=4)
        return (int(resp[1:], 10) + 1) * 2

    @accel.setter
    def accel(self, value):
        value = int(value / 2 - 1)
        if not (0 <= value <= 4):
            raise ValueError('Bad acceleration value')
        self.send('a{0:03d}'.format(value))

    @property
    def maxvel(self):
        """
        The maximum rotator velocity in deg/s
        """
        resp = self.send('?004', feedback=4)
        return self.degrees_per_sec(int(resp[1:], 10))

    @maxvel.setter
    def maxvel(self, value):
        value = self.counts_per_sec(value)
        if not (0 < value <= 80):
            raise ValueError('Bad velocity value')
        self.send('m{0:03d}'.format(value))

    @property
    def is_moving(self):
        """
        ``True`` if rotator is moving.
        """
        resp = self.send('?007', feedback=4)
        return resp[1:] == '001'

    @property
    def has_stalled(self):
        """
        ``True`` if rotator has stalled or slipped.
        """
        resp = self.send('?005', feedback=4)
        return resp[1:] == '001'

    @property
    def brake(self):
        """
        The rotator brake setting.
        """
        resp = self.send('?006', feedback=4)
        return int(resp[1:], 10)

    @property
    def flags(self):
        """
        The rotator motion and stall status as a bitmask. Bit 0 is
        the motion status and Bit 1 is the stall status.
        """
        m_stat = self.send('?007', feedback=4)
        s_stat = self.send('?005', feedback=4)
        return int(s_stat[1:], 10) << 1 | int(m_stat[1:], 10)


class Light(Node):
    """
    Interface to an ROS underwater light
    """
    cal = Calibration([
        Decimal('1.1164014655e-03'),
        Decimal('2.3798297321e-04'),
        Decimal('-3.72283234e-07'),
        Decimal('9.9063233e-08')], '')
    prec = Decimal('0.1')

    @staticmethod
    def rtherm(ad_val):
        """
        Convert the raw A/D value read from the light to a thermistor
        resistance value.

        :param ad_val:
        :return: resistance in ohms
        """
        return (10.24e6 / ad_val) - 10000

    @property
    def intensity(self):
        """
        The light intensity setting.
        """
        resp = self.send('?005', feedback=5)
        return int(resp[2:], 10)

    @intensity.setter
    def intensity(self, val):
        self.send('l{0:03d}'.format(val))

    @property
    def temperature(self):
        """
        Return the Light temperature in degrees C using the equations found
        in Section 5.7.2.3 of the ROS Communication Protocol manual.

        :return: temperature value
        :rtype: decimal.Decimal
        """
        resp = self.send('f', feedback=4)
        ad_val = int(resp[1:], 10)
        x = math.log(Light.rtherm(ad_val))
        y, _ = Light.cal(Decimal(str(x)))
        temp = Decimal(1) / y - Decimal('273.15')
        return temp.quantize(Light.prec)
