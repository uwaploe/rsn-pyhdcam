#!/usr/bin/env python
#
"""
.. :module: system
     :synopsis: Manage the system configuration
"""
import yaml
import time
import gevent
import logging
from gevent.queue import Queue
from decimal import Decimal, getcontext
from contextlib import contextmanager
from collections import namedtuple
from UserDict import UserDict
from pyhdcam.serialcom import Port
from pyhdcam.ncd import RelayBank, read_adc10
from pyhdcam.utils import Calibration
from pyhdcam.ros import Rotator
from pyhdcam.tcm6 import Tcm6
from pyhdcam.light import Light
from pyhdcam.camera import Camera, Commands

#: Map names to serial ports
Ports = {}
#: List of A/D channels
Adc = [None] * 8
#: Map names to relay numbers
Relays = UserDict()
#: ncd.RelayBank
Relays._rb = None


class PanTilt(object):
    """
    Class to manage the pan/tilt rotators and the associated
    attitude sensor.
    """
    Feedback = namedtuple('Feedback',
                          'pan tilt heading pitch roll pan_status tilt_status')

    def __init__(self, cfg, nosensor=False):
        """
        :param cfg: configuration dictionary.
        :param nosensor: if ``True``, the attitude sensor is not
                         used. All angles are measured in the rotator
                         coordinate system.
        """
        self.sensor = None
        self.task = None
        self.logger = logging.getLogger('pan/tilt')
        Relays._rb[Relays['pan_tilt']] = 1
        gevent.sleep(cfg['pan_tilt']['warmup'])
        Ports['pan_tilt'].open()
        # Create each Rotator instance and set the operational parameters.
        for node in ('pan', 'tilt'):
            self.logger.debug('Initializing %s rotator', node)
            setattr(self, node, Rotator(Ports['pan_tilt'],
                                        node_id=cfg['pan_tilt']['ids'][node]))
            obj = getattr(self, node)
            obj.apply_brake(cfg['pan_tilt']['brake'])
            obj.accel = cfg['pan_tilt']['accel']
            obj.maxvel = cfg['pan_tilt']['maxvel']
            ulimits = cfg['pan_tilt']['limits'].get(node)
            if ulimits:
                obj.ccw = ulimits[0]
                obj.cw = ulimits[1]
        # The scale factors are used to convert between the angles measured
        # by the attitude sensor and those measured by the rotators.
        self.scale_pitch, self.scale_heading = -1, 1
        if nosensor or ('attitude' not in cfg):
            self.sensor = None
            self.logger.debug('TCM6 not used')
        else:
            self.logger.debug('Initializing TCM6')
            Relays._rb[Relays['attitude']] = 1
            gevent.sleep(cfg['attitude']['warmup'])
            Ports['attitude'].open()
            self.sensor = Tcm6(Ports['attitude'], timeout=3)
            # Try to read a sample from the attitude sensor
            try:
                result = self.sensor.kGetData()
            except StopIteration:
                self.logger.critical('TCM6 not responding')
                result = None
            if result and result.id == 'kDataResp':
                self.sensor.kSetConfig(id='kMountingRef',
                                       value=cfg['attitude']['orientation'])
            else:
                self.logger.debug('Disabling TCM6')
                Relays._rb[Relays['attitude']] = 0
                self.sensor = None

    def __del__(self):
        Relays._rb[Relays['pan_tilt']] = 0
        if self.sensor:
            Relays._rb[Relays['attitude']] = 0
            Ports['attitude'].close()
        Ports['pan_tilt'].close()
        if self.task and not self.task.ready():
            self.task.kill(timeout=3)

    def _worker(self, pan_angle, tilt_angle, dataq, interval):
        """
        Greenlet task to manage adjusting the pan and tilt.
        """
        t = time.time()
        try:
            # Start the rotators in motion
            if pan_angle >= 0:
                self.pan.moveto(pan_angle)
            if tilt_angle >= 0:
                self.tilt.moveto(tilt_angle)
        except Exception as e:
            dataq.put((t, e))
        else:
            # Monitor the rotator motion and write the status
            # information to the queue every *interval* seconds.
            try:
                t = time.time()
                fb = self.feedback()
                while (fb.pan_status & 1) or (fb.tilt_status & 1):
                    dataq.put((t, fb))
                    gevent.sleep(interval)
                    t = time.time()
                    fb = self.feedback()
            except gevent.GreenletExit:
                dataq.put(StopIteration)
            except Exception as e:
                dataq.put((time.time(), e))
            else:
                dataq.put((t, fb))
        dataq.put(StopIteration)

    def feedback(self):
        """
        Check the position and status of the rotators. The position values
        are gravity and magnetic North referenced if the attitude sensor is
        used, otherwise they are in the rotators' coordinate system.

        :rtype: PanTilt.Feedback
        """
        if self.sensor:
            result = self.sensor.kGetData()
            return PanTilt.Feedback(pan=self.pan.angle,
                                    tilt=self.tilt.angle,
                                    heading=round(result.payload.kHeading, 1),
                                    pitch=round(result.payload.kPAngle, 1),
                                    roll=round(result.payload.kRAngle, 1),
                                    pan_status=self.pan.flags,
                                    tilt_status=self.tilt.flags)
        else:
            return PanTilt.Feedback(pan=self.pan.angle,
                                    tilt=self.tilt.angle,
                                    heading=self.pan.angle,
                                    pitch=self.tilt.angle,
                                    roll=0.,
                                    pan_status=self.pan.flags,
                                    tilt_status=self.tilt.flags)

    def adjust(self, pan_angle=None, tilt_angle=None, speed=0):
        """
        Adjust the Pan/Tilt position.

        The adjustment is performed asynchronously and the returned data
        queue should be read by the caller to monitor the status of the
        operation. Each value from the queue is a tuple containing
        a timestamp and either a PanTilt.Feedback object or an Exception.

        The queue supports the iterator protocol so it may be read in a
        loop::

            for t, status in pt.adjust(pan, tilt, speed):
                if isinstance(status, Exception):
                    # Handle exception ...
                else:
                    # Process feedback ...

        :param pan_angle: desired pan rotator position in degrees.
        :param tilt_angle: desired tilt rotator position in degrees
        :param speed: speed in *deg/s*, if 0, use the most recent speed
                      setting.
        :rtype: gevent.queue.Queue
        """
        dataq = Queue()
        pan, tilt = -1, -1
        if pan_angle is not None:
            pan = pan_angle
            if speed > 0:
                self.pan.maxvel = speed
        if tilt_angle is not None:
            tilt = tilt_angle
            if speed > 0:
                self.tilt.maxvel = speed
        self.logger.debug('Starting pan/tilt adjustment (%f, %f)', pan, tilt)
        self.task = gevent.spawn(self._worker, pan, tilt, dataq, 1)
        return dataq

    def stop(self):
        """
        Stop any in-progress position adjustment.
        """
        if self.task and not self.task.ready():
            self.task.kill(timeout=3)


class Lights(object):
    """
    Manage the LED light system.
    """
    def __init__(self, cfg, names=None):
        """
        :param cfg: configuration dictionary.
        :param names: names of lights to enable, by default, all
                      available lights are used.
        """
        self.devs = []
        self.names = []
        keys = names or ['light_1', 'light_2']
        for key in keys:
            if key:
                Relays._rb[Relays[key]] = 1
                gevent.sleep(cfg[key].get('warmup', 0.5))
                Ports[key].open()
                dev = Light(Ports[key])
                dev.flush_input()
                self.devs.append(dev)
                self.names.append(key)

    def __del__(self):
        for key in self.names:
            Ports[key].close()
            Relays._rb[Relays[key]] = 0

    def flash(self, ontime, intensity):
        """
        Flash all of the lights.

        :param ontime: number of seconds to stay on
        :param intensity: light intensity in percent.
        """
        self.intensity = intensity
        gevent.sleep(ontime)
        self.intensity = [0] * len(self.devs)

    @property
    def intensity(self):
        """
        Light intensities in percent.
        """
        vals = []
        for dev in self.devs:
            dev.flush_input()
            percent = int((dev.intensity * 100. / 255.) + 0.5)
            vals.append(percent)
        return vals

    @intensity.setter
    def intensity(self, vals):
        for dev, percent in zip(self.devs, vals):
            val = int((percent * 255. / 100.) + 0.5)
            dev.flush_input()
            dev.intensity = val


class VideoCam(object):
    """
    Manage video camera interface.
    """
    def __init__(self, cfg):
        """
        :param cfg: configuration dictionary.
        """
        Relays._rb[Relays['camera']] = 1
        gevent.sleep(cfg['camera']['warmup'])
        Ports['camera'].open()
        self.logger = logging.getLogger('camera')
        self.dev = Camera(Ports['camera'])
        self.dev.flush_input()
        settings = self.dev.read_settings()
        self.force_off = cfg['camera'].get('force_off') is not None
        if not settings:
            self.logger.critical('Cannot read camera settings')
        # Ensure that the camera is in HD mode.
        elif self.dev.get('hd') == '0':
            self.dev.send(Commands.TOGGLE_VIDEO, timeout=5)

    def __del__(self):
        Ports['camera'].close()
        if self.force_off:
            Relays._rb[Relays['camera']] = 0

    def __getattr__(self, name):
        return getattr(self.dev, name)

    def __getitem__(self, name):
        return self.dev[name]

    def __setitem__(self, name, val):
        self.dev[name] = val


def tovolts(x):
    """
    Convert A/D counts to volts.

    :param x: A/D value in counts
    :return: volts
    """
    return (Decimal(x) * tovolts.scale).quantize(tovolts.exp)
tovolts.scale = Decimal(1)
tovolts.exp = Decimal('0.001')


def load_configuration(filename):
    """
    Load the YAML configuration file for the HDCAM system and initialize the
    global data structures.

    :param filename:
    :return: configuration dictionary
    """
    cfg = yaml.load(open(filename, 'r'))
    # Initialize all of the serial ports
    for component in cfg:
        pcfg = cfg[component].get('port')
        if pcfg:
            p = Port(pcfg['device'], baud=pcfg['baud'])
            Ports[component] = p
    # Load all of the ADC calibration information
    adc = cfg['relayboard']['adc']
    for obj in adc['channels']:
        Adc[obj['index']] = {
            'name': obj.get('name', 'Ain{0:d}'.format(obj['index'])),
            'cal': Calibration([Decimal(c) for c in obj['cal']], obj['units'])
        }
    vs = float(adc['vrange'][1] - adc['vrange'][0]) / ((1 << adc['bits']) - 1)
    tovolts.scale = Decimal(str(vs))
    # Map names to relay numbers
    Ports['relayboard'].open()
    Relays._rb = RelayBank(Ports['relayboard'])
    for obj in cfg['relayboard']['relays']:
        Relays[obj['name']] = obj['index']
    return cfg


def readadc(n=1):
    """
    Read all of the configured A/D channels and return the result as a list
    of three-element tuples: *name*, *value*, *units*

    :param n: number of samples to average.
    """
    getcontext().prec = 4
    result = []
    nchans = len(Adc)
    accum = [0] * nchans
    port = Ports.get('relayboard')
    if port:
        for i in range(n):
            values = read_adc10(port)
            for j in range(nchans):
                accum[j] += values[j]
        values = [x // n for x in accum]
        for i, x in enumerate(values):
            adc = Adc[i]
            if adc is not None:
                y, units = adc['cal'](tovolts(x))
                result.append((adc['name'], y, units))
            else:
                result.append((
                    'Ain{0:d}'.format(i),
                    tovolts(x), 'volts'
                ))
    return result


def power_on(name):
    """
    Switch a relay on.

    :param name: relay name
    """
    idx = Relays[name]
    Relays._rb[idx] = 1


def power_off(name=None):
    """
    Switch a relay off. If *name* is ``None``, switch off all relays.

    :param name: relay name
    """
    if name is None:
        Relays._rb(0)
    else:
        idx = Relays[name]
        Relays._rb[idx] = 0


def power_state(name):
    """
    Check a relay state.

    :param name: relay name
    :return: 1 (on) or 0 (off)
    """
    return Relays._rb[Relays[name]]


def switches():
    """
    Return a list of relay names.
    """
    return Relays.keys()


@contextmanager
def session(cfg, **kwds):
    """
    Manage an HD-Camera session by powering-on the Pan/Tilt, lights, and
    camera then returing control to the caller. On exit, everything is
    powered-down.
    """
    pt = PanTilt(cfg, **kwds)
    lights = Lights(cfg)
    cam = VideoCam(cfg)
    try:
        yield pt, lights, cam
    finally:
        time.sleep(1)
        if cfg['camera'].get('force_off'):
            Relays._rb(0)
        else:
            Relays._rb(0x01)
