#!/usr/bin/env python
#
"""
.. module:: serialcom
   :platform: Unix
   :synopsis: Gevent compatible serial port interface.
"""
import serial
import os
import time
from gevent.select import select


class Port(object):
    """
    Serial port interface. Enhances a ``serial.Serial`` instance to work with
    gevent.
    """
    def __init__(self, device, baud=9600):
        """
        :param device: serial device filename (e.g. ``/dev/ttyS0``)
        :param baud: baud rate
        :type baud: integer
        """
        #: Optional file to log the serial port I/O
        self.sessionlog = None
        #: Serial port interface
        self.serial = serial.Serial()
        #: File descriptor for serial port interface
        self.fd = None
        self.serial.port = device
        self.serial.baudrate = baud
        self.serial.timeout = 0

    def __repr__(self):
        return '%s(%r, baud=%r)' % (
            self.__class__.__name__,
            self.serial.port, self.serial.baudrate)

    def open(self, sessionlog=None):
        """
        Open the serial interface and start a communication session.

        :param sessionlog: file to log all communication.
        """
        if self.fd is not None:
            return
        self.sessionlog = sessionlog
        self.serial.open()
        self.serial.nonblocking()
        self.fd = self.serial.fileno()
        if self.sessionlog:
            self.sessionlog.write('<session class="%s" dev="%s">\n' % (
                self.__class__.__name__,
                self.serial.port.encode('utf-8')))

    def isopen(self):
        """
        :return: ``True`` if the port is open.
        """
        return self.fd is not None

    def close(self):
        """
        Close the serial interface and communication session.
        """
        if self.isopen():
            self.serial.close()
            self.fd = None
            if self.sessionlog:
                self.sessionlog.write('</session>\n')
                self.sessionlog.flush()

    def read(self, nbytes, timeout=None):
        """
        Read data from the serial port until *nbytes* are received or the
        *timeout* expires. A *timeout* value of ``None`` results in a
        blocking read.

        :param nbytes: number of bytes to read
        :param timeout: serial port timeout in seconds
        :return: byte string
        """
        if self.fd is None:
            raise IOError('Serial port not open')
        n = 0
        buf = []
        while n < nbytes:
            rlist, _, _ = select([self.fd], [], [], timeout=timeout)
            if not rlist:
                break
            s = os.read(self.fd, nbytes - n)
            if not s:
                break
            n += len(s)
            buf.append(s)
            if self.sessionlog:
                self.sessionlog.write(
                    '<recv t="%.6f">%r</recv>\n' % (time.time(), s))
        return ''.join(buf)

    def write(self, s, timeout=None):
        """
        Write a sequence of bytes to the serial port

        :param s: string or byte sequence
        :param timeout: serial port timeout in seconds
        :return: sequence of unwritten bytes, will be empty
                 if the operation was successful.
        """
        if self.fd is None:
            raise IOError('Serial port is not open')
        s = serial.to_bytes(s)
        while s:
            _, wlist, _ = select([], [self.fd], [], timeout=timeout)
            if not wlist:
                break
            n = os.write(self.fd, s)
            if n <= 0:
                break
            if self.sessionlog:
                self.sessionlog.write(
                    '<send t="%.6f">%r</send>\n' % (time.time(), s[:n]))
            s = s[n:]
        return s

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, *args):
        self.close()
