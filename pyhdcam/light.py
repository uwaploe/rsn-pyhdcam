#!/usr/bin/env python
#
"""
.. module:: light
   :synopsis: Interface to SubC Control underwater lights
"""


class Light(object):
    """
    Interface to a SubC LED light with adjustable intensity.
    """
    def __init__(self, port, writeonly=True):
        """
        Initialize a new class instance.

        .. note::

           port must be open.

        :param port: interface to serial port.
        :type port: serialcom.Port
        """
        assert port.isopen()
        self.port = port
        self.writeonly = writeonly
        self.value = 0

    def __repr__(self):
        return '{0}({1!r})'.format(self.__class__.__name__, self.port)

    def readline(self, timeout=None, eol='\n'):
        """
        Read the output from the device up to the next linefeed.

        :param timeout: read timeout in seconds or ``None`` to block.
        :rtype: string or ``None``
        """
        text = []
        done = False
        while not done:
            c = self.port.read(1, timeout=timeout)
            if not c:
                return None
            if c == eol:
                done = True
            else:
                text.append(c)
        return ''.join(text)

    def flush_input(self):
        """
        Flush all content from the input buffer.
        """
        data = self.port.read(128, timeout=0)
        while data:
            data = self.port.read(128, timeout=0)

    @property
    def intensity(self):
        """
        The light intensity setting.
        """
        if self.writeonly:
            return self.value
        self.port.write('$?\r\n', timeout=3)
        resp = self.readline(2)
        while resp is not None:
            if resp.startswith("Dim="):
                break
            resp = self.readline(2)

        if resp is None:
            raise IOError('No response from {0!r}'.format(self))
        try:
            self.flush_input()
            _, val = resp.split('=')
        except ValueError:
            raise IOError('Unexpected response: %r' % resp)
        return int(val.strip(), 10)

    @intensity.setter
    def intensity(self, val):
        self.port.write('$d{0:03d}\r\n'.format(val), timeout=3)
        self.value = val
        resp = self.readline(3)
        while resp is not None:
            if resp.startswith('@'):
                break
            resp = self.readline(1)
        self.flush_input()
