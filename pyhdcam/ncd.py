#!/usr/bin/env python
#
"""
.. module:: ncd
   :synopsis: Interface to NCD relay and analog input controllers
"""
import struct


class Command(object):
    """
    ProXR command codes
    """
    START = chr(254)
    READ_BANK = chr(124)
    WRITE_BANK = chr(140)
    STORE_BANK = chr(142)
    RECALL_BANK = chr(143)
    READ_ALL_ADC_10BIT = chr(167)
    TEST = chr(33)

    @classmethod
    def RELAY_OFF(cls, relay):
        assert 0 <= relay < 8
        return chr(100 + relay)

    @classmethod
    def RELAY_ON(cls, relay):
        assert 0 <= relay < 8
        return chr(108 + relay)

    @classmethod
    def RELAY_STATUS(cls, relay):
        assert 0 <= relay < 8
        return chr(116 + relay)

    @classmethod
    def READ_ADC_10BIT(cls, channel):
        assert 0 <= channel < 8
        return chr(158 + channel)

#: Response code from most ProXR commands.
PROXR_RESPONSE = chr(85)


def proxr_command(port, cmd, args=None, nresp=0, timeout=3):
    """
    Send a ProXR command and wait for a response.

    :param port: interface to a serial port
    :type port: serialcom.Port
    :param cmd: command string
    :param args: list of arguments (1 character strings)
    :param nresp: number of characters in the response
    :param timeout: number of seconds to wait for a response
    :return: response string
    """
    data = args or []
    port.write(Command.START, timeout=timeout)
    for c in cmd:
        port.write(c, timeout=timeout)
    for c in data:
        port.write(c, timeout=timeout)
    if nresp:
        return port.read(nresp, timeout=timeout)
    else:
        return None


def read_adc10(port):
    """
    Read all 8 channels of a 10-bit A/D converter on an NCD device.

    :param port: interface to a serial port
    :type port: serialcom.Port
    :rtype: tuple of 8 integers (0-1023)
    :raise: :class:`IOError`
    """
    assert port.isopen()
    result = proxr_command(port, Command.READ_ALL_ADC_10BIT, nresp=16)
    if len(result) != 16:
        raise IOError('Invalid response to ADC read ({0})'.format(
            repr(result)))
    else:
        return struct.unpack('>8H', result)


class RelayBank(object):
    """
    Class to represent a bank of 8 NCD relays. This class emulates a list
    to allow access to each individual relay or a callable to allow access
    to all of the relays simultaneously::

        rb = RelayBank(port)
        # Turn on relay 1
        rb[1] = 1
        # Turn off relay 2
        rb[2] = 0
        # Turn relays 0-3 on and 4-7 off
        rb(0x0f)
        # Read the state of all relays
        state = rb()

    """
    n_relays = 8

    def __init__(self, port, bank=1):
        """
        Initialize a class instance.

        .. note::

           port must be open.

        :param port: serial port interface
        :param bank: relay bank number (1-32)
        """
        assert port.isopen()
        self.port = port
        self.bank = bank

    def __repr__(self):
        return 'RelayBank({0!r}, {1:d})'.format(self.port, self.bank)

    def __getitem__(self, item):
        """
        Read the state of a relay

        :param item: relay number (0-7)
        :return: 1 if relay is on, 0 if off
        :raise: IndexError
        """
        if self.bank == 0:
            raise IndexError
        if isinstance(item, slice):
            indicies = item.indices(len(self))
            return [self[i] for i in range(*indicies)]
        else:
            if not (0 <= item < self.n_relays):
                raise IndexError
            return ord(proxr_command(self.port, Command.RELAY_STATUS(item),
                                     args=[chr(self.bank)], nresp=1))

    def __setitem__(self, key, value):
        """
        Set the state of a relay.

        :param key: relay number (0-7)
        :param value: 1 to set the relay, 0 to clear
        """
        if isinstance(key, slice):
            indicies = key.indices(len(self))
            for i, v in zip(range(*indicies), value):
                self[i] = v
        else:
            if value:
                proxr_command(self.port, Command.RELAY_ON(key),
                              args=[chr(self.bank)], nresp=1)
            else:
                proxr_command(self.port, Command.RELAY_OFF(key),
                              args=[chr(self.bank)], nresp=1)

    def __len__(self):
        return self.n_relays

    def __int__(self):
        """
        Read the state of all relays.

        :return: relay state as an integer value (0-255)
        :raise: ValueError
        """
        if self.bank == 0:
            raise ValueError
        x = proxr_command(self.port, Command.READ_BANK,
                          args=[chr(self.bank)], nresp=1)
        return ord(x)

    def __call__(self, state=None):
        """
        Read or write the state of all relays.

        :param state: if not ``None`` specifies new relay state (0-255)
        :return: current relay state as an integer value (0-255)
        :raise: ValueError
        """
        if self.bank == 0:
            raise ValueError
        if state is not None:
            assert 0 <= state < 256
            proxr_command(self.port, Command.WRITE_BANK,
                          args=[chr(state), chr(self.bank)], nresp=1)
        x = proxr_command(self.port, Command.READ_BANK,
                          args=[chr(self.bank)], nresp=1)
        return ord(x)

    def is_alive(self):
        """
        Test the serial communication link.

        :return: True if the link is up.
        """
        x = proxr_command(self.port, Command.TEST, nresp=1)
        return x == PROXR_RESPONSE
